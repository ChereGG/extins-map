package socialnetwork.gui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import socialnetwork.service.*;

import java.lang.reflect.Method;

public class LoginGui extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{


        FXMLLoader loader=new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/LoginView.fxml"));
        AnchorPane root=loader.load();
        Method method = loader.getController().getClass().getMethod("init");
        method.invoke(loader.getController());
        primaryStage.setScene(new Scene(root, 700, 500));
        primaryStage.setTitle("Autentificare");
        primaryStage.show();

    }

    public static void main(String[] args) {
        launch(args);
    }

}