package socialnetwork.gui.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import org.w3c.dom.Text;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.RepoException;
import socialnetwork.domain.validators.ServiceException;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.service.UtilizatorService;

public class ControllerSignUp {

    @FXML
    private TextField idNumeTextField;
    @FXML
    private TextField idPrenumeTextField;
    @FXML
    private TextField idUsernameTextField;
    @FXML
    private TextField idPasswordField;
    @FXML
    private Button idSignUpButton;
    private UtilizatorService utilizatorService;

    public void init(UtilizatorService utilizatorService) {
        this.utilizatorService = utilizatorService;
    }

    public void signUpClickedEvent(MouseEvent mouseEvent) {
        String nume=this.idNumeTextField.getText();
        String prenume=this.idPrenumeTextField.getText();
        String username=this.idUsernameTextField.getText();
        String password=this.idPasswordField.getText();
        if(nume.equals("")||prenume.equals("")||username.equals("")||password.equals("")){
            Alert alert = new Alert(Alert.AlertType.ERROR, "Nu pot ramane campuri vide!", ButtonType.OK);
            alert.showAndWait();
        }
        else{
            Utilizator u=(Utilizator) this.utilizatorService.findOneDataBase(username);
            if(u!=null){
                Alert alert = new Alert(Alert.AlertType.ERROR, "Acest username exista deja!", ButtonType.OK);
                alert.showAndWait();
            }
            else {
                Utilizator utilizator = new Utilizator(nume, prenume, username, password);
                try {
                    this.utilizatorService.addUtilizator(utilizator);
                }
                catch(RepoException| ServiceException| ValidationException ex){
                    Alert alert = new Alert(Alert.AlertType.ERROR, ex.getMessage(), ButtonType.OK);
                    alert.showAndWait();
                }
            }
        }
        this.idNumeTextField.clear();
        this.idPrenumeTextField.clear();
        this.idUsernameTextField.clear();
        this.idPasswordField.clear();
    }
}
