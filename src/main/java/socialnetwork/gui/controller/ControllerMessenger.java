package socialnetwork.gui.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.util.Callback;
import socialnetwork.domain.*;
import socialnetwork.service.*;

import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class ControllerMessenger implements Observer {
    private PrietenieService prietenieService;
    private UtilizatorService utilizatorService;
    private MessageService messageService;
    private ConversatieService conversatieService;
    private CereriPrietenieService cereriPrietenieService;
    private GrupService grupService;
    private EvenimenteService evenimenteService;
    private Long idUtilizator;
    private Utilizator lastClickedUser = null;
    private Grup lastClickedGroup = null;
    private GrupMessageService grupMessageService;
    @FXML
    private ImageView pictureId;
    @FXML
    private ListView<String> idListChat;
    @FXML
    private TextArea idTextBar;
    @FXML
    private Button idSendButton;
    @FXML
    private Button createGroupId;
    @FXML
    private TableView<Utilizator> usersTableId;
    @FXML
    private TableColumn<Utilizator, ImageView> usersPictureId;
    @FXML
    private TableColumn<Utilizator, String> usersLastNameId;
    @FXML
    private TableColumn<Utilizator, String> usersFirstNameId;
    @FXML
    private TableView<Grup> groupsTableId;
    @FXML
    private TableColumn<Grup, ImageView> groupsPictureId;
    @FXML
    private TableColumn<Grup, String> groupsNameId;
    @FXML
    private TextField searchConversationsId;
    @FXML
    private Button idButtonPrev;
    @FXML
    private Button idButtonNext;
    @FXML
    private Button idExitButton;
    private ObservableList<Utilizator> modelUsers = FXCollections.observableArrayList();
    private ObservableList<Grup> modelGroups = FXCollections.observableArrayList();

    public void init(UtilizatorService utilizatorService, PrietenieService prietenieService, MessageService messageService, Long idUtilizator, ConversatieService conversatieService, GrupService grupService, GrupMessageService grupMessageService, CereriPrietenieService cereriPrietenieService,EvenimenteService evenimenteService) {
        this.idSendButton.setVisible(false);

        this.usersTableId.getColumns().forEach(x -> {
            x.setResizable(false);
            x.setReorderable(false);
        });
        this.groupsTableId.getColumns().forEach(x -> {
            x.setResizable(false);
            x.setReorderable(false);
        });
        this.prietenieService = prietenieService;
        this.utilizatorService = utilizatorService;
        this.messageService = messageService;
        this.idUtilizator = idUtilizator;
        this.grupService = grupService;
        this.conversatieService = conversatieService;
        this.grupMessageService = grupMessageService;
        this.cereriPrietenieService = cereriPrietenieService;
        this.evenimenteService=evenimenteService;
        this.conversatieService.addObserver(this);
        this.utilizatorService.addObserver(this);
        this.grupService.addObserver(this);
        this.utilizatorService.addObserver(this);
        this.prietenieService.addObserver(this);
        this.loadModelGroups(this.grupService.toateGrupurileUtilizator(idUtilizator));
        this.incarcaStatusMesaje();
        this.loadModelUsers(this.utilizatorService.findOne(idUtilizator).getFriends().stream()
                .map(x -> this.utilizatorService.findOne(x))
                .collect(Collectors.toList())
        );
        this.usersTableId.setItems(modelUsers);
        this.groupsTableId.setItems(modelGroups);
        this.usersPictureId.setCellValueFactory(new PropertyValueFactory<Utilizator, ImageView>("imageView"));
        this.usersLastNameId.setCellValueFactory(new PropertyValueFactory<Utilizator, String>("lastName"));
        this.usersLastNameId.setCellFactory(new Callback<TableColumn<Utilizator, String>, TableCell<Utilizator, String>>() {
            @Override
            public TableCell<Utilizator, String> call(TableColumn<Utilizator, String> param) {
                return new TableCell<Utilizator, String>() {
                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);

                        if (isEmpty()) {
                            setText("");
                        } else {

                            setTextFill(Color.BLACK);
                            setFont(Font.font("Calibri", 20));
                            setAlignment(Pos.CENTER);
                            setTextAlignment(TextAlignment.CENTER);
                            setText(item);
                        }
                    }
                };
            }
        });
        this.usersFirstNameId.setCellValueFactory(new PropertyValueFactory<Utilizator, String>("firstName"));
        this.usersFirstNameId.setCellFactory(new Callback<TableColumn<Utilizator, String>, TableCell<Utilizator, String>>() {
            @Override
            public TableCell<Utilizator, String> call(TableColumn<Utilizator, String> param) {
                return new TableCell<Utilizator, String>() {
                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);

                        if (isEmpty()) {
                            setText("");
                        } else {

                            setTextFill(Color.BLACK);
                            setFont(Font.font("Calibri", 20));
                            setAlignment(Pos.CENTER);
                            setTextAlignment(TextAlignment.CENTER);
                            setText(item);
                        }
                    }
                };
            }
        });
        this.groupsPictureId.setCellValueFactory(new PropertyValueFactory<Grup, ImageView>("imageView"));
        this.groupsNameId.setCellValueFactory(new PropertyValueFactory<Grup, String>("groupName"));
        this.groupsNameId.setCellFactory(new Callback<TableColumn<Grup, String>, TableCell<Grup, String>>() {
            @Override
            public TableCell<Grup, String> call(TableColumn<Grup, String> param) {
                return new TableCell<Grup, String>() {
                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);

                        if (isEmpty()) {
                            setText("");
                        } else {

                            setTextFill(Color.BLACK);
                            setFont(Font.font("Calibri", 20));
                            setAlignment(Pos.CENTER);
                            setTextAlignment(TextAlignment.CENTER);
                            setText(item);
                        }
                    }
                };
            }
        });

    }

    private void loadModelUsers(List<Utilizator> list) {
        modelUsers.setAll(list);
    }

    private void loadModelGroups(List<Grup> list) {
        modelGroups.setAll(list);
    }

    public void sendMessageEvent(MouseEvent mouseEvent) {
        String s = this.idTextBar.getText();
        String s2 = s.strip();
        if (!s2.equals("")) {
            if (this.lastClickedUser != null) {
                Conversatie conversatie = new Conversatie(idUtilizator, this.lastClickedUser.getId(), s2, LocalDateTime.now());
                conversatie.setId(null);
                this.conversatieService.addConversatie(conversatie);
                String s1 = this.utilizatorService.findOne(idUtilizator).getFirstName();
                s1 += " : ";
                s1 += s2;
                this.idListChat.getItems().add(s1);
                this.idTextBar.clear();
                this.idSendButton.setVisible(false);
            } else if (this.lastClickedGroup != null) {
                GrupMessage grupMessage = new GrupMessage(idUtilizator, this.lastClickedGroup.getId(), s2, LocalDateTime.now());
                grupMessage.setId(null);
                this.grupMessageService.addMessage(grupMessage);
                String s1 = this.utilizatorService.findOne(idUtilizator).getFirstName();
                s1 += " : ";
                s1 += s2;
                this.idListChat.getItems().add(s1);
                this.idTextBar.clear();
                this.idSendButton.setVisible(false);

            }
        }
    }

    public void usersRowClickedEvent(MouseEvent mouseEvent) {
        this.idListChat.getItems().clear();
        Utilizator utilizator = usersTableId.getSelectionModel().getSelectedItem();
        if (utilizator != null) {
            ArrayList<Conversatie> list = this.conversatieService.toateMesajele(idUtilizator, utilizator.getId());
            list.forEach(x -> {
                String s = "";
                s += this.utilizatorService.findOne(x.getFrom()).getFirstName() + " : ";
                s += x.getMessage();
                this.idListChat.getItems().add(s);
            });

            this.lastClickedUser = utilizator;
            this.lastClickedGroup = null;
            this.pictureId.setImage(new Image(utilizator.getPicture()));
            this.pictureId.setVisible(true);
            if (!this.idTextBar.getText().strip().equals("")) {
                this.idSendButton.setVisible(true);
            }
        }
    }

    public void groupsRowClickedEvent(MouseEvent mouseEvent) {
        this.idListChat.getItems().clear();
        Grup grup = this.groupsTableId.getSelectionModel().getSelectedItem();
        if (grup != null) {
            List<GrupMessage> list = this.grupMessageService.toateMesajeleDinGrup(grup.getId());
            list.forEach(x -> {
                String s = "";
                Utilizator u=(Utilizator)this.utilizatorService.findOneDataBaseId(x.getFrom());
                s += u.getFirstName() + " : ";
                s += x.getMessage();
                this.idListChat.getItems().add(s);
            });
            this.lastClickedUser = null;
            this.lastClickedGroup = grup;
            this.pictureId.setImage(new Image(grup.getPicturePath()));
            this.pictureId.setVisible(true);
            if (!this.idTextBar.getText().strip().equals("")) {
                this.idSendButton.setVisible(true);
            }
        }
    }

    public void createGroupEvent(MouseEvent mouseEvent) {
        try {
            Stage raporarte = new Stage();
            FXMLLoader loader2 = new FXMLLoader();
            loader2.setLocation(getClass().getResource("/view/NewGroupView.fxml"));
            AnchorPane root = loader2.load();
            Method method = loader2.getController().getClass().getMethod("init", PrietenieService.class,ConversatieService.class,UtilizatorService.class,Long.class,MessageService.class,GrupService.class,GrupMessageService.class,EvenimenteService.class,CereriPrietenieService.class);
            method.invoke(loader2.getController(),prietenieService,conversatieService,utilizatorService,idUtilizator,messageService,grupService,grupMessageService,evenimenteService,cereriPrietenieService);
            Scene scene = new Scene(root,522.0 , 678.0);
            raporarte.setScene(scene);
            raporarte.setTitle("Table Raports");
            raporarte.setResizable(false);
            raporarte.show();
            Stage stage = (Stage) this.idExitButton.getScene().getWindow();
            stage.close();
        }
        catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        this.loadModelUsers(this.utilizatorService.findOne(idUtilizator).getFriends().stream()
                .map(x -> this.utilizatorService.findOne(x))
                .collect(Collectors.toList())
        );
        this.loadModelGroups(this.grupService.toateGrupurileUtilizator(idUtilizator));

    }

    public void searchBarTypeEvent(KeyEvent keyEvent) {
        this.loadModelUsers(StreamSupport.stream(utilizatorService.getAll().spliterator(), false)
                .filter(x -> {
                    String s = x.getFirstName() + " " + x.getLastName();
                    String s2 = x.getLastName() + " " + x.getFirstName();
                    return s.startsWith(this.searchConversationsId.getText()) || s2.startsWith(this.searchConversationsId.getText());
                })
                .collect(Collectors.toList())
        );
        this.loadModelGroups(StreamSupport.stream(this.grupService.getAll().spliterator(), false)
                .filter(x -> {
                    String s = x.getGroupName();
                    return s.contains(this.searchConversationsId.getText());
                })
                .collect(Collectors.toList())
        );

    }

    public void searchBarClickedEvent(MouseEvent mouseEvent) {
        this.loadModelUsers(StreamSupport.stream(utilizatorService.getAll().spliterator(), false)
                .filter(x -> {
                    String s = x.getFirstName() + " " + x.getLastName();
                    String s2 = x.getLastName() + " " + x.getFirstName();
                    return s.startsWith(this.searchConversationsId.getText()) || s2.startsWith(this.searchConversationsId.getText());
                })
                .collect(Collectors.toList())
        );
        this.loadModelGroups(StreamSupport.stream(this.grupService.getAll().spliterator(), false)
                .filter(x -> {
                    String s = x.getGroupName();
                    return s.contains(this.searchConversationsId.getText());
                })
                .collect(Collectors.toList())
        );
    }

    public void messageBarTypeEvent(KeyEvent keyEvent) {
        String s = this.idTextBar.getText().strip();
        if ((this.lastClickedUser != null || this.lastClickedGroup != null) && (!s.equals(""))) {
            this.idSendButton.setVisible(true);
        }
        if (this.idTextBar.getText().strip().equals("")) {
            this.idSendButton.setVisible(false);
        }
    }

    private void incarcaStatusMesaje() {
        this.prietenieService.DOOM();
        this.conversatieService.DOOM();
        //this.cereriPrietenieService.DOOM();
        Utilizator logat = (Utilizator) this.utilizatorService.findOneDataBaseId(this.idUtilizator);
        boolean ok = this.utilizatorService.incarcaPrimiiPrieteni(logat);
        this.utilizatorService.addSmecher(logat);
        this.utilizatorService.getAll().forEach(x -> {
            if (!x.getId().equals(idUtilizator)) {
                this.prietenieService.incarcaPrietenieDacaExista(logat, x);
                this.conversatieService.incarcaConversatieDacaExista(logat, x);
                //this.cereriPrietenieService.incarcaCerereDacaExista(logat,x);
            }
        });
        this.utilizatorService.reloadFriends();
        if (ok) {
            this.idButtonNext.setVisible(true);
        }
    }

    public void prevEvent(MouseEvent mouseEvent) {
        this.lastClickedUser = null;
        this.lastClickedGroup = null;
        this.idListChat.getItems().clear();
        this.pictureId.setVisible(false);
        this.idSendButton.setVisible(false);
        Utilizator LastOne = this.usersTableId.getItems().get(0);
        Utilizator logat = (Utilizator) this.utilizatorService.findOneDataBaseId(this.idUtilizator);
        boolean ok = this.utilizatorService.incarcaAnterioriiPrieteni(logat, LastOne);
        this.utilizatorService.addSmecher(logat);
        this.rearanjare(ok, logat);
        this.idButtonNext.setVisible(true);
        if (!ok) {
            this.idButtonPrev.setVisible(false);
        }
    }

    public void nextEvent(MouseEvent mouseEvent) {
        this.lastClickedUser = null;
        this.lastClickedGroup = null;
        this.idListChat.getItems().clear();
        this.pictureId.setVisible(false);
        this.idSendButton.setVisible(false);
        Utilizator LastOne = this.usersTableId.getItems().get(this.usersTableId.getItems().size() - 1);
        Utilizator logat = (Utilizator) this.utilizatorService.findOneDataBaseId(this.idUtilizator);
        boolean ok = this.utilizatorService.incarcaUrmatoriiPrieteni(logat, LastOne);
        this.utilizatorService.addSmecher(logat);
        this.rearanjare(ok, logat);
        this.idButtonPrev.setVisible(true);
        if (!ok) {
            this.idButtonNext.setVisible(false);
        }
    }


    private void rearanjare(boolean ok, Utilizator logat) {
        this.prietenieService.DOOM();
        // this.cereriPrietenieService.DOOM();
        this.conversatieService.DOOM();
        this.utilizatorService.getAll().forEach(x -> {
            if (!x.getId().equals(idUtilizator)) {
                this.prietenieService.incarcaPrietenieDacaExista(logat, x);
                // this.cereriPrietenieService.incarcaCerereDacaExista(logat, x);
                this.conversatieService.incarcaConversatieDacaExista(logat, x);
            }
        });
        this.utilizatorService.reloadFriends();
        this.loadModelUsers(StreamSupport.stream(utilizatorService.getAll().spliterator(), false)
                .filter(x -> x.getId() != idUtilizator)
                .collect(Collectors.toList()));

    }

    public void exitEvent(MouseEvent mouseEvent) {
        try {
            Stage utilizator = new Stage();
            FXMLLoader loader2 = new FXMLLoader();
            loader2.setLocation(getClass().getResource("/view/UtilizatorView.fxml"));
            AnchorPane root = loader2.load();
            Method method = loader2.getController().getClass().getMethod("init", PrietenieService.class, UtilizatorService.class, MessageService.class, CereriPrietenieService.class, Long.class, ConversatieService.class, GrupService.class, GrupMessageService.class,EvenimenteService.class);
            method.invoke(loader2.getController(), prietenieService, utilizatorService, messageService, cereriPrietenieService, idUtilizator, conversatieService, grupService, grupMessageService,evenimenteService);
            Scene scene = new Scene(root, 862.0, 777.0);
            utilizator.setScene(scene);
            utilizator.setTitle("Social Network");
            utilizator.setResizable(false);
            utilizator.show();
            Stage stage = (Stage) idExitButton.getScene().getWindow();
            stage.close();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}
