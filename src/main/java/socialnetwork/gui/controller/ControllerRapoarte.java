package socialnetwork.gui.controller;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.scene.SnapshotParameters;
import javafx.scene.chart.BubbleChart;
import javafx.scene.chart.PieChart;
import javafx.scene.control.*;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import socialnetwork.NodeToPdf;
import socialnetwork.domain.Conversatie;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Utilizator;
import socialnetwork.service.ConversatieService;
import socialnetwork.service.MessageService;
import socialnetwork.service.PrietenieService;
import socialnetwork.service.UtilizatorService;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ControllerRapoarte {
    private PrietenieService prietenieService;
    private ConversatieService conversatieService;
    private UtilizatorService utilizatorService;
    Long idUtilizator;
    @FXML PieChart idChart;
    @FXML RadioButton idRaportPrieteni;
    @FXML RadioButton idRaportMesaje;
    @FXML Button  idExportPdf;
    @FXML DatePicker idBeginDate;
    @FXML DatePicker idEndDate;
    public void init(PrietenieService prietenieService, ConversatieService messageService, Long idUtilizator,UtilizatorService utilizatorService) {
        this.prietenieService = prietenieService;
        this.conversatieService = messageService;
        this.idUtilizator = idUtilizator;
        this.utilizatorService=utilizatorService;
        this.idBeginDate.setValue(LocalDate.now());
        this.idEndDate.setValue(LocalDate.now());
        this.idBeginDate.setEditable(false);
        this.idEndDate.setEditable(false);
        this.prietenieService.incarcaToatePrieteniile((Utilizator)this.utilizatorService.findOneDataBaseId(idUtilizator));
        this.conversatieService.incarcaToateConversatiile((Utilizator)this.utilizatorService.findOneDataBaseId(idUtilizator));
    }

    public void RaportPrieteniEvent(MouseEvent mouseEvent) {

            this.idChart.setVisible(true);
            this.idChart.getData().clear();
            this.idRaportMesaje.setSelected(false);
            int numarBun = this.prietenieService.numarPrieteniiIntervalData(idUtilizator, this.idBeginDate.getValue(), this.idEndDate.getValue());
            Collection<Prietenie> z=(Collection< Prietenie>)this.prietenieService.getAll();
            int numarTotal = z.size();
            PieChart.Data slice1 = new PieChart.Data("Prietenii in perioada aleasa", numarBun);
            PieChart.Data slice2 = new PieChart.Data("Restul prieteniilor", numarTotal - numarBun);
            this.idChart.getData().add(slice1);
            this.idChart.getData().add(slice2);
            this.idChart.setTitle("Raport Prietenii");
        }


        public void RaportMesajeEvent (MouseEvent mouseEvent){


                this.idChart.setVisible(true);
                this.idChart.setTitle("Raport Mesaje");
                this.idChart.getData().clear();
                this.idRaportPrieteni.setSelected(false);
                ArrayList<Conversatie> convos = this.conversatieService.toateMesajeleIntervalData(idUtilizator, this.idBeginDate.getValue(), this.idEndDate.getValue());
                Map<Long, Integer> map = new HashMap<>();
                convos.forEach(x -> {
                    if (map.containsKey(x.getFrom())) {
                        map.put(x.getFrom(), map.get(x.getFrom()) + 1);
                    } else {
                        map.put(x.getFrom(), 1);
                    }
                });
                map.forEach((x, y) -> {
                    Utilizator u=((Utilizator)this.utilizatorService.findOneDataBaseId(x));
                    PieChart.Data slice = new PieChart.Data(u.getFirstName() + " " + u.getLastName(), y);
                    this.idChart.getData().add(slice);
                });
            }



    public void ExportPdfEvent(MouseEvent mouseEvent) throws IOException, URISyntaxException, DocumentException {
        WritableImage nodeshot = this.idChart.snapshot(new SnapshotParameters(), null);
        File file = new File("chart.png");



        try {
            ImageIO.write(SwingFXUtils.fromFXImage(nodeshot, null), "png", file);
            System.out.println(file.getPath() + " das " + file.getAbsolutePath());

            Path path = Paths.get(file.toURL().toURI());

            if(idRaportPrieteni.isSelected()) {
                Document document = new Document();
                PdfWriter.getInstance(document, new FileOutputStream("RaportPrietenii.pdf"));
                document.open();
                Paragraph paragraph = new Paragraph();
                Font font = FontFactory.getFont(FontFactory.COURIER_BOLD,20);
                Chunk chunk = new Chunk("Raport Prietenii",font);
                Paragraph paragraph1 = new Paragraph(chunk);
                paragraph1.setAlignment(Element.ALIGN_CENTER);
                paragraph.add(paragraph1);
                paragraph.add(new Paragraph(" "));
                paragraph.add(new Paragraph(" "));
                paragraph.add(new Paragraph(" "));
                paragraph.add(new Paragraph(" "));
                Utilizator u = this.utilizatorService.findOne(idUtilizator);
                paragraph.add(new Paragraph("Raportul pentru utilizatorul: " +
                        u.getFirstName() +
                        " " +
                        u.getLastName() +
                        " privind prieteniile create incepand din data: " +
                        this.idBeginDate.getValue().format(DateTimeFormatter.ofPattern("dd.MM.yy")) +
                        " pana la data: " +
                        this.idEndDate.getValue().format(DateTimeFormatter.ofPattern("dd.MM.yyyy"))+
                        "  Va multumim pentru incredere!"));
                Image img = Image.getInstance(path.toAbsolutePath().toString());
                document.add(paragraph);
                document.add(img);

                document.close();
                Alert alert = new Alert(Alert.AlertType.INFORMATION, "Raport salvat!", ButtonType.OK);
                alert.showAndWait();
            }
            else if(this.idRaportMesaje.isSelected())
            {
                Document document = new Document();
                PdfWriter.getInstance(document, new FileOutputStream("RaportMesaje.pdf"));
                document.open();
                Paragraph paragraph = new Paragraph();
                Font font = FontFactory.getFont(FontFactory.COURIER_BOLD,20);
                Chunk chunk = new Chunk("Raport Mesaje",font);
                Paragraph paragraph1 = new Paragraph(chunk);
                paragraph1.setAlignment(Element.ALIGN_CENTER);
                paragraph.add(paragraph1);
                paragraph.add(new Paragraph(" "));
                paragraph.add(new Paragraph(" "));
                paragraph.add(new Paragraph(" "));
                paragraph.add(new Paragraph(" "));
                Utilizator u = this.utilizatorService.findOne(idUtilizator);
                paragraph.add(new Paragraph("Raportul pentru utilizatorul: " +
                        u.getFirstName() +
                        " " +
                        u.getLastName() +
                        " privind mesajele primite incepand din data: " +
                        this.idBeginDate.getValue().format(DateTimeFormatter.ofPattern("dd.MM.yy")) +
                        " pana la data: " +
                        this.idEndDate.getValue().format(DateTimeFormatter.ofPattern("dd.MM.yyyy"))+
                        "  Va multumim pentru incredere!"));
                Image img = Image.getInstance(path.toAbsolutePath().toString());
                document.add(paragraph);
                document.add(img);

                document.close();
                Alert alert = new Alert(Alert.AlertType.INFORMATION, "Raport salvat!", ButtonType.OK);
                alert.showAndWait();
            }
            else {
                Alert alert = new Alert(Alert.AlertType.ERROR, "Selecteaza un raport!", ButtonType.OK);
                alert.showAndWait();
            }
            file.delete();
        } catch (IOException e) {
            System.out.println(e);
        }

    }
}
