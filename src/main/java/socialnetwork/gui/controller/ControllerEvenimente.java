package socialnetwork.gui.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.util.Callback;
import socialnetwork.domain.Event;
import socialnetwork.domain.Utilizator;
import socialnetwork.service.EvenimenteService;
import socialnetwork.service.UtilizatorService;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class ControllerEvenimente {
    private EvenimenteService evenimenteService;
    private UtilizatorService utilizatorService;
    Long idUtilizator;
    Long lastIdEventClicked;
    @FXML private TableView<Event>idEventTable;
    @FXML private TableColumn<Event, ImageView>idPictureEvent;
    @FXML private TableColumn<Event, String>idNameEvent;
    @FXML private TableColumn<Event, String>idDataEvent;
    @FXML private Button myEventsButtonId;
    @FXML private Button allEventsButtonId;
    @FXML private Button idSubscribeButton;
    @FXML private Button idUnsubscribeButton;
    @FXML private ImageView idPozaEvent;
    @FXML private Label idNumeEventLabel;
    @FXML private Label idDataEventLabel;
    @FXML private Circle notifyCircle;
    @FXML private Label notifyLabelId;
    @FXML private Label descriereEventId;
    private ObservableList<Event> model = FXCollections.observableArrayList();
    public void init(UtilizatorService utilizatorService,EvenimenteService evenimenteService,Long idUtilizator) {
        this.evenimenteService = evenimenteService;
        this.utilizatorService = utilizatorService;
        this.idUtilizator=idUtilizator;
        this.idEventTable.getColumns().forEach(x->{
            x.setResizable(false);
            x.setReorderable(false);
        });
        this.loadModel(StreamSupport.stream(evenimenteService.getAll().spliterator(), false)
                .collect(Collectors.toList()));
        this.idEventTable.setItems(model);
        this.idPictureEvent.setCellValueFactory(new PropertyValueFactory<>("imageView"));
        this.idNameEvent.setCellValueFactory(new PropertyValueFactory<>("nume"));
        this.idDataEvent.setCellValueFactory(new PropertyValueFactory<>("stringData"));
        this.idNameEvent.setCellFactory(new Callback<TableColumn<Event, String>, TableCell<Event, String>>() {
            @Override
            public TableCell<Event, String> call(TableColumn<Event, String> param) {
                return new TableCell<Event,String>(){
                    @Override
                    public void updateItem(String item, boolean empty){
                        super.updateItem(item, empty);

                        if(isEmpty())
                        {
                            setText("");
                        }
                        else
                        {

                            setTextFill(Color.BLACK);
                            setFont(Font.font ("Calibri", 20));
                            setAlignment(Pos.CENTER);
                            setTextAlignment(TextAlignment.CENTER);
                            setText(item);
                        }
                    }
                };
            }
        });
        this.idDataEvent.setCellFactory(new Callback<TableColumn<Event, String>, TableCell<Event, String>>() {
            @Override
            public TableCell<Event, String> call(TableColumn<Event, String> param) {
                return new TableCell<Event,String>(){
                    @Override
                    public void updateItem(String item, boolean empty){
                        super.updateItem(item, empty);

                        if(isEmpty())
                        {
                            setText("");
                        }
                        else
                        {

                            setTextFill(Color.BLACK);
                            setFont(Font.font ("Calibri", 20));
                            setAlignment(Pos.CENTER);
                            setTextAlignment(TextAlignment.CENTER);
                            setText(item);
                        }
                    }
                };
            }
        });
        List<Event> lista=StreamSupport.stream(evenimenteService.getAll().spliterator(), false)
                .filter(x->x.getParticipanti().contains(idUtilizator))
                .collect(Collectors.toList());
        if(lista.size()>0){
            this.notifyCircle.setVisible(true);
            if(lista.size()<=9){
                this.notifyLabelId.setText(String.valueOf(lista.size()));
            }
            else
            {
                this.notifyLabelId.setText("9+");
            }
            this.notifyLabelId.setVisible(true);
        }


    }
    private void loadModel(List<Event> list)
    {
        model.setAll(list);
    }
    public void tableClickedEvent(MouseEvent mouseEvent) {
        Event event=this.idEventTable.getSelectionModel().getSelectedItem();
        this.idPozaEvent.setImage(new Image(event.getPicturePath()));
        this.idNumeEventLabel.setText(event.getNume());
        this.idDataEventLabel.setText(event.getStringData());
        this.descriereEventId.setText(event.getDescriere());
        this.descriereEventId.setMaxWidth(200);
        this.descriereEventId.setWrapText(true);
        this.lastIdEventClicked=event.getId();
        this.idPozaEvent.setVisible(true);
        this.idNumeEventLabel.setVisible(true);
        this.idDataEventLabel.setVisible(true);
        this.descriereEventId.setVisible(true);
        if(event.getParticipanti().contains(idUtilizator)){
            this.idSubscribeButton.setVisible(false);
            this.idUnsubscribeButton.setVisible(true);
        }
        else{
            this.idSubscribeButton.setVisible(true);
            this.idUnsubscribeButton.setVisible(false);
        }
    }

    public void myEventsId(MouseEvent mouseEvent) {
        this.loadModel(StreamSupport.stream(evenimenteService.getAll().spliterator(), false)
                .filter(x->x.getParticipanti().contains(idUtilizator))
                .collect(Collectors.toList()));
        this.notifyCircle.setVisible(false);
        this.notifyLabelId.setVisible(false);
    }

    public void allEventsClickedEvent(MouseEvent mouseEvent) {
        this.loadModel(StreamSupport.stream(evenimenteService.getAll().spliterator(), false)
                .collect(Collectors.toList()));
    }

    public void subscribeEvent(MouseEvent mouseEvent) {
        Event event=this.evenimenteService.findOne(this.lastIdEventClicked);
        List<Long> list =event.getParticipanti();
        list.add(idUtilizator);
        Event event1=new Event(event.getNume(),event.getData(),list,event.getPicturePath(),event.getImageView(),event.getDescriere());
        event1.setId(event.getId());
        this.evenimenteService.updateEvent(event1);
        this.idSubscribeButton.setVisible(false);
        this.idUnsubscribeButton.setVisible(true);

    }

    public void unsubscribeEvent(MouseEvent mouseEvent) {
        Event event=this.evenimenteService.findOne(this.lastIdEventClicked);
        List<Long> list =event.getParticipanti();
        list.remove(idUtilizator);
        Event event1=new Event(event.getNume(),event.getData(),list,event.getPicturePath(),event.getImageView(),event.getDescriere());
        event1.setId(event.getId());
        this.evenimenteService.updateEvent(event1);
        this.idSubscribeButton.setVisible(true);
        this.idUnsubscribeButton.setVisible(false);
    }
}
