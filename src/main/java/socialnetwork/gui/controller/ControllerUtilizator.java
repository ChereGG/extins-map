package socialnetwork.gui.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import socialnetwork.domain.*;
import socialnetwork.service.*;

import java.io.File;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


public class ControllerUtilizator implements Observer {
    private PrietenieService prietenieService;
    private UtilizatorService utilizatorService;
    private MessageService messageService;
    private CereriPrietenieService cereriPrietenieService;
    private ConversatieService conversatieService;
    private GrupService grupService;
    private GrupMessageService grupMessageService;
    private EvenimenteService evenimenteService;
    private Long idUtilizator;
    private Long lastIdShown;
    private Utilizator logat;
    private String filtr;
    private String UltimulButonImportantApasat;
    @FXML
    private Button idButtonProfile;
    @FXML
    private Button idButtonMessenger;
    @FXML
    private ImageView logoutPictureId;
    @FXML
    private TextField searchBarId;
    @FXML
    private TableView<Utilizator> tableId;
    @FXML
    private TableColumn<Utilizator,ImageView> pictureColumnId;
    @FXML
    private TableColumn<Utilizator,String> numeColumnId;
    @FXML
    private TableColumn<Utilizator,String> prenumeColumnId;
    @FXML
    private ImageView profilePictureId;
    @FXML
    private Label nameLabelId;
    @FXML
    private Button addFriendButtonId;
    @FXML
    private Button removeFriendButtonId;
    @FXML
    private Button removeRequestButtonId;
    @FXML
    private Button changePictureButtonId;
    @FXML
    private Button idButtonFriendRequests;
    @FXML
    private Button acceptButtonId;
    @FXML
    private Button idEventsButton;
    @FXML
    private Button declineButtonId;
    @FXML
    private Button idButtonRapoarte1;
    @FXML
    private Button getIdButtonRapoarte2;
    @FXML
    private Button idButtonPrevTabel;
    @FXML
    private Button idButtonNextTabel;
    private ObservableList<Utilizator> model = FXCollections.observableArrayList();

    public void init(PrietenieService prietenieService, UtilizatorService utilizatorService, MessageService messageService, CereriPrietenieService cereriPrietenieService,Long id,ConversatieService conversatieService,GrupService grupService,GrupMessageService grupMessageService,EvenimenteService evenimenteService) {
        this.prietenieService = prietenieService;
        this.utilizatorService = utilizatorService;
        this.messageService = messageService;
        this.cereriPrietenieService = cereriPrietenieService;
        this.idUtilizator=id;
        this.grupService=grupService;
        this.conversatieService=conversatieService;
        this.grupMessageService=grupMessageService;
        this.evenimenteService=evenimenteService;
        this.tableId.getColumns().forEach(x->{
            x.setResizable(false);
            x.setReorderable(false);
        });
        Utilizator u=(Utilizator)this.utilizatorService.findOneDataBaseId(this.idUtilizator);
        this.nameLabelId.setText(u.getFirstName()+" "+u.getLastName());
        this.incarcaStatusPrieteni();
        this.profilePictureId.setImage(new Image(u.getPicture()));
        this.loadModel(this.utilizatorService.findOne(idUtilizator).getFriends().stream()
                .map(x -> this.utilizatorService.findOne(x))
                .collect(Collectors.toList())
        );
        this.tableId.setItems(model);
        this.UltimulButonImportantApasat="friends";

        this.pictureColumnId.setCellValueFactory(new PropertyValueFactory<Utilizator,ImageView>("imageView"));
        this.numeColumnId.setCellValueFactory(new PropertyValueFactory<Utilizator,String>("lastName"));
        this.numeColumnId.setCellFactory(new Callback<TableColumn<Utilizator, String>, TableCell<Utilizator, String>>() {
            @Override
            public TableCell<Utilizator, String> call(TableColumn<Utilizator, String> param) {
                return new TableCell<Utilizator,String>(){
                    @Override
                    public void updateItem(String item, boolean empty){
                        super.updateItem(item, empty);

                        if(isEmpty())
                        {
                            setText("");
                        }
                        else
                        {

                            setTextFill(Color.BLACK);
                            setFont(Font.font ("Calibri", 20));
                            setAlignment(Pos.CENTER);
                            setTextAlignment(TextAlignment.CENTER);
                            setText(item);
                        }
                    }
                };
            }
        });
        this.prenumeColumnId.setCellValueFactory(new PropertyValueFactory<Utilizator,String>("firstName"));
        this.prenumeColumnId.setCellFactory(new Callback<TableColumn<Utilizator, String>, TableCell<Utilizator, String>>() {
            @Override
            public TableCell<Utilizator, String> call(TableColumn<Utilizator, String> param) {
                return new TableCell<Utilizator,String>(){
                    @Override
                    public void updateItem(String item, boolean empty){
                        super.updateItem(item, empty);

                        if(isEmpty())
                        {
                            setText("");
                        }
                        else
                        {

                            setTextFill(Color.BLACK);
                            setFont(Font.font ("Calibri", 20));
                            setAlignment(Pos.CENTER);
                            setTextAlignment(TextAlignment.CENTER);
                            setText(item);
                        }
                    }
                };
            }
        });
        this.utilizatorService.addObserver(this);
        this.prietenieService.addObserver(this);
        this.cereriPrietenieService.addObserver(this);
    }

    private void loadModel(List<Utilizator> list)
    {
        model.setAll(list);
    }



    public void myProfileEvent(MouseEvent mouseEvent) {
        Utilizator u=this.utilizatorService.findOne(this.idUtilizator);
        this.nameLabelId.setText(u.getFirstName()+" "+u.getLastName());
        this.profilePictureId.setImage(new Image(u.getPicture()));
        this.changePictureButtonId.setVisible(true);
        this.addFriendButtonId.setVisible(false);
        this.removeFriendButtonId.setVisible(false);
        this.removeRequestButtonId.setVisible(false);
        this.acceptButtonId.setVisible(false);
        this.declineButtonId.setVisible(false);
    }

    public void messengerEvent(MouseEvent mouseEvent) {
        try {
            Stage messenger = new Stage();
            FXMLLoader loader2 = new FXMLLoader();
            loader2.setLocation(getClass().getResource("/view/MessengerView.fxml"));
            AnchorPane root = loader2.load();
            Method method = loader2.getController().getClass().getMethod("init", UtilizatorService.class, PrietenieService.class,MessageService.class,Long.class,ConversatieService.class,GrupService.class,GrupMessageService.class,CereriPrietenieService.class,EvenimenteService.class);
            method.invoke(loader2.getController(), utilizatorService,prietenieService,messageService,idUtilizator,conversatieService,grupService,grupMessageService,cereriPrietenieService,evenimenteService);
            Scene scene = new Scene(root,1081.0 , 761.0);
            messenger.setScene(scene);
            messenger.setTitle("Messenger");
            messenger.setResizable(false);
            messenger.show();
            Stage stage = (Stage) this.logoutPictureId.getScene().getWindow();
            stage.close();
        }
        catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }

    public void friendsEvent(MouseEvent mouseEvent) {
        this.idButtonNextTabel.setVisible(false);
        this.idButtonPrevTabel.setVisible(false);
        this.incarcaStatusPrieteni();
        this.loadModel(this.utilizatorService.findOne(idUtilizator).getFriends().stream()
                .map(x -> this.utilizatorService.findOne(x))
                .collect(Collectors.toList())
        );
        this.UltimulButonImportantApasat="friends";
    }

    public void logoutEvent(MouseEvent mouseEvent) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/view/LoginView.fxml"));
            AnchorPane root = loader.load();
            Stage primaryStage = new Stage();
            Method method = loader.getController().getClass().getMethod("init");
            method.invoke(loader.getController());
            primaryStage.setScene(new Scene(root, 700, 500));
            primaryStage.setTitle("Autentificare");
            primaryStage.show();
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
        Stage stage = (Stage) this.logoutPictureId.getScene().getWindow();
        stage.close();
    }

    public void searchEvent(KeyEvent keyEvent) {
    }

    public void addFriendEvent(MouseEvent mouseEvent) {
        CererePrietenie cererePrietenie = new CererePrietenie();
        cererePrietenie.setId(new Tuple<>(idUtilizator,lastIdShown));
        this.cereriPrietenieService.addCererePrietenie(cererePrietenie);
        this.changePictureButtonId.setVisible(false);
        this.addFriendButtonId.setVisible(false);
        this.removeFriendButtonId.setVisible(false);
        this.removeRequestButtonId.setVisible(true);
        this.acceptButtonId.setVisible(false);
        this.declineButtonId.setVisible(false);
    }

    public void removeFriendEvent(MouseEvent mouseEvent) {
        this.prietenieService.deletePrietenie(new Tuple<>(idUtilizator,lastIdShown));
        this.changePictureButtonId.setVisible(false);
        this.addFriendButtonId.setVisible(true);
        this.removeFriendButtonId.setVisible(false);
        this.removeRequestButtonId.setVisible(false);
        this.acceptButtonId.setVisible(false);
        this.declineButtonId.setVisible(false);
    }

    public void removeRequestEvent(MouseEvent mouseEvent) {
        this.cereriPrietenieService.deleteCererePrietenie(new Tuple<>(idUtilizator,lastIdShown));
        this.changePictureButtonId.setVisible(false);
        this.addFriendButtonId.setVisible(true);
        this.removeFriendButtonId.setVisible(false);
        this.removeRequestButtonId.setVisible(false);
        this.acceptButtonId.setVisible(false);
        this.declineButtonId.setVisible(false);
    }

    public void changePictureEvent(MouseEvent mouseEvent) {
        Stage stage = new Stage();
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                new FileChooser.ExtensionFilter("PNG", "*.png")
        );
        fileChooser.setTitle("Select profile picture");
        File file = fileChooser.showOpenDialog(stage);
        String imageUrl = null;
        if (file != null) {

            try {
                imageUrl = file.toURI().toURL().toExternalForm();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            Image image = new Image(imageUrl);
        }
/*        String path = file.getPath();
        System.out.println(imageUrl);*/
        Utilizator utilizator2 = utilizatorService.findOne(idUtilizator);
        Utilizator utilizator = new Utilizator(utilizator2.getFirstName(),utilizator2.getLastName(),imageUrl,new ImageView(new Image(imageUrl)),utilizator2.getHashing(),utilizator2.getUsername());
        utilizator.setId(utilizator2.getId());
        utilizator.setFriends(utilizator2.getFriends());
        utilizatorService.updateUtilizator(utilizator);
        this.profilePictureId.setImage(new Image(imageUrl));
        ImageView imageView = new ImageView(new Image(imageUrl));
        imageView.setFitWidth(40);
        imageView.setFitHeight(40);
        this.utilizatorService.findOne(idUtilizator).setImageView(imageView);

    }


    @Override
    public void update(Observable o, Object arg) {
        List<Utilizator> lista2 = FXCollections.observableArrayList();
        this.tableId.getItems().forEach(x -> {
            if(x!=null) {
                lista2.add(this.utilizatorService.findOne(x.getId()));
            }
        });
        this.loadModel(lista2);
    }

    public void friendRequestEvent(MouseEvent mouseEvent) {
        this.idButtonNextTabel.setVisible(false);
        this.idButtonPrevTabel.setVisible(false);
        this.incarcaStatusCereri();
        this.loadModel(this.cereriPrietenieService.cereriDePrieteniePending(idUtilizator).stream()
            .map(x -> {
                return this.utilizatorService.findOne(x.getId().getLeft());
            })
            .collect(Collectors.toList())
        );
        this.UltimulButonImportantApasat="requests";
    }

    public void acceptRequestEvent(MouseEvent mouseEvent) {
        CererePrietenie cererePrietenie1 = new CererePrietenie(Status.APPROVED);
        cererePrietenie1.setId(new Tuple<>(lastIdShown,idUtilizator));
        cereriPrietenieService.updateCererePrietenie(cererePrietenie1);
        Prietenie prietenie = new Prietenie();
        prietenie.setId(new Tuple<>(lastIdShown,idUtilizator));
        this.prietenieService.addPrietenie(prietenie);
        this.changePictureButtonId.setVisible(false);
        this.addFriendButtonId.setVisible(false);
        this.removeFriendButtonId.setVisible(true);
        this.removeRequestButtonId.setVisible(false);
        this.acceptButtonId.setVisible(false);
        this.declineButtonId.setVisible(false);

    }

    public void declineRequestEvent(MouseEvent mouseEvent) {
        CererePrietenie cererePrietenie1 = new CererePrietenie(Status.REJECTED);
        cererePrietenie1.setId(new Tuple<>(lastIdShown,idUtilizator));
        cereriPrietenieService.updateCererePrietenie(cererePrietenie1);
        this.changePictureButtonId.setVisible(false);
        this.addFriendButtonId.setVisible(true);
        this.removeFriendButtonId.setVisible(false);
        this.removeRequestButtonId.setVisible(false);
        this.acceptButtonId.setVisible(false);
        this.declineButtonId.setVisible(false);
    }

    public void clickedSearchEvent(MouseEvent mouseEvent) {
        this.searchBarId.clear();
        this.UltimulButonImportantApasat="clickSearch";
        this.idButtonNextTabel.setVisible(false);
        this.idButtonPrevTabel.setVisible(false);
        this.incarcaStatusUtilizatori();
        this.loadModel(StreamSupport.stream(utilizatorService.getAll().spliterator(),false)
                .filter(x -> {
                    String s = x.getFirstName() + " " + x.getLastName();
                    String s2 = x.getLastName() + " " + x.getFirstName();
                    return s.startsWith(searchBarId.getText()) || s2.startsWith(searchBarId.getText());
                })
                .collect(Collectors.toList())
        );

    }

    public void tableRowClickedEvent(MouseEvent mouseEvent) {
        Utilizator utilizator =  tableId.getSelectionModel().getSelectedItem();
        if(utilizator != null)
        {
            this.lastIdShown = utilizator.getId();
            this.nameLabelId.setText(utilizator.getFirstName()+" "+utilizator.getLastName());
            this.profilePictureId.setImage(new Image(utilizator.getPicture()));
            this.changePictureButtonId.setVisible(false);
            if(utilizator.getId().equals(idUtilizator))
            {

                this.changePictureButtonId.setVisible(true);
                this.addFriendButtonId.setVisible(false);
                this.removeFriendButtonId.setVisible(false);
                this.removeRequestButtonId.setVisible(false);
                this.acceptButtonId.setVisible(false);
                this.declineButtonId.setVisible(false);
            }
            else if(utilizator.getFriends().contains(idUtilizator)){

                this.addFriendButtonId.setVisible(false);
                this.removeFriendButtonId.setVisible(true);
                this.removeRequestButtonId.setVisible(false);
                this.acceptButtonId.setVisible(false);
                this.declineButtonId.setVisible(false);
            }
            else if(this.cereriPrietenieService.aPrimitCerere(idUtilizator, utilizator.getId()))
            {

                this.addFriendButtonId.setVisible(false);
                this.removeFriendButtonId.setVisible(false);
                this.removeRequestButtonId.setVisible(false);
                this.acceptButtonId.setVisible(true);
                this.declineButtonId.setVisible(true);
            }
            else if(this.cereriPrietenieService.aPrimitCerere(utilizator.getId(),idUtilizator))
            {

                this.addFriendButtonId.setVisible(false);
                this.removeFriendButtonId.setVisible(false);
                this.removeRequestButtonId.setVisible(true);
                this.acceptButtonId.setVisible(false);
                this.declineButtonId.setVisible(false);
            }
            else{
                this.addFriendButtonId.setVisible(true);
                this.removeFriendButtonId.setVisible(false);
                this.removeRequestButtonId.setVisible(false);
                this.acceptButtonId.setVisible(false);
                this.declineButtonId.setVisible(false);
            }
        }
    }

    public void Rapoarte1Event(MouseEvent mouseEvent) {
        try {
            Stage raporarte = new Stage();
            FXMLLoader loader2 = new FXMLLoader();
            loader2.setLocation(getClass().getResource("/view/RapoarteView.fxml"));
            AnchorPane root = loader2.load();
            Method method = loader2.getController().getClass().getMethod("init", PrietenieService.class,ConversatieService.class,Long.class,UtilizatorService.class);
            method.invoke(loader2.getController(),prietenieService,conversatieService,idUtilizator,utilizatorService);
            Scene scene = new Scene(root,904.0 , 606.0);
            raporarte.setScene(scene);
            raporarte.setTitle("PieChart Raports");
            raporarte.setResizable(false);
            raporarte.show();
        }
        catch (Exception ex){
            System.out.println(ex.getMessage());
        }

    }

    public void Rapoarte2Event(MouseEvent mouseEvent) {
        try {
            Stage raporarte = new Stage();
            FXMLLoader loader2 = new FXMLLoader();
            loader2.setLocation(getClass().getResource("/view/TabelView.fxml"));
            AnchorPane root = loader2.load();
            Method method = loader2.getController().getClass().getMethod("init", PrietenieService.class,ConversatieService.class,UtilizatorService.class,Long.class,MessageService.class,GrupService.class,GrupMessageService.class,EvenimenteService.class,CereriPrietenieService.class);
            method.invoke(loader2.getController(),prietenieService,conversatieService,utilizatorService,idUtilizator,messageService,grupService,grupMessageService,evenimenteService,cereriPrietenieService);
            Scene scene = new Scene(root,889.0 , 679.0);
            raporarte.setScene(scene);
            raporarte.setTitle("Table Raports");
            raporarte.setResizable(false);
            raporarte.show();
            Stage stage = (Stage) this.logoutPictureId.getScene().getWindow();
            stage.close();
        }
        catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }

    private void incarcaStatusUtilizatori(){
        AtomicReference<Utilizator> logat=new AtomicReference<>();
        logat.set((Utilizator)this.utilizatorService.findOneDataBaseId(this.idUtilizator));
        boolean ok=this.utilizatorService.incarcaPrimii(logat.get());
        this.prietenieService.DOOM();
        this.cereriPrietenieService.DOOM();
        this.utilizatorService.getAll().forEach(x->{
            if(!x.getId().equals(idUtilizator)){
                this.prietenieService.incarcaPrietenieDacaExista(logat.get(),x);
                this.cereriPrietenieService.incarcaCerereDacaExista(logat.get(),x);
            }
        });
        this.utilizatorService.reloadFriends();
        if(ok){
            this.idButtonNextTabel.setVisible(true);
        }
    }
    private void incarcaStatusPrieteni(){
        this.prietenieService.DOOM();
        this.cereriPrietenieService.DOOM();
        Utilizator logat=(Utilizator)this.utilizatorService.findOneDataBaseId(this.idUtilizator);
        boolean ok=this.utilizatorService.incarcaPrimiiPrieteni(logat);
        this.utilizatorService.addSmecher(logat);
        this.utilizatorService.getAll().forEach(x->{
            if(!x.getId().equals(idUtilizator)){
                this.prietenieService.incarcaPrietenieDacaExista(logat,x);
                this.cereriPrietenieService.incarcaCerereDacaExista(logat,x);
            }
        });
        this.utilizatorService.reloadFriends();
        if(ok){
            this.idButtonNextTabel.setVisible(true);
        }
    }

    private void incarcaStatusCereri(){
        this.prietenieService.DOOM();
        this.cereriPrietenieService.DOOM();
        Utilizator logat=(Utilizator)this.utilizatorService.findOneDataBaseId(this.idUtilizator);
        boolean ok=this.utilizatorService.incarcaPrimiiCereri(logat);
        this.utilizatorService.addSmecher(logat);
        this.utilizatorService.getAll().forEach(x->{
            if(!x.getId().equals(idUtilizator)){
                this.prietenieService.incarcaPrietenieDacaExista(logat,x);
                this.cereriPrietenieService.incarcaCerereDacaExista(logat,x);
            }
        });
        this.utilizatorService.reloadFriends();
        if(ok){
            this.idButtonNextTabel.setVisible(true);
        }
    }

    private void incarcaStatusUtilizatoriFiltrati(){
        AtomicReference<Utilizator> logat=new AtomicReference<>();
        logat.set((Utilizator)this.utilizatorService.findOneDataBaseId(this.idUtilizator));
        boolean ok=this.utilizatorService.incarcaPrimiiFiltrati(logat.get(),filtr);
        this.prietenieService.DOOM();
        this.cereriPrietenieService.DOOM();
        this.utilizatorService.getAll().forEach(x->{
            if(!x.getId().equals(idUtilizator)){
                this.prietenieService.incarcaPrietenieDacaExista(logat.get(),x);
                this.cereriPrietenieService.incarcaCerereDacaExista(logat.get(),x);
            }
        });
        this.utilizatorService.reloadFriends();
        if(ok){
            this.idButtonNextTabel.setVisible(true);
        }
    }


    public void previousEvent(MouseEvent mouseEvent) {
        if (this.UltimulButonImportantApasat.equals("clickSearch")) {
            Utilizator LastOne = this.tableId.getItems().get(1);
            Utilizator logat = (Utilizator) this.utilizatorService.findOneDataBaseId(this.idUtilizator);
            boolean ok = this.utilizatorService.incarcaPrev(LastOne, logat);
            this.rearanjare(ok,logat);
            this.idButtonNextTabel.setVisible(true);
            if (!ok) {
                this.idButtonPrevTabel.setVisible(false);
            }
        }
        else if (this.UltimulButonImportantApasat.equals("friends")) {
                Utilizator LastOne = this.tableId.getItems().get(0);
                Utilizator logat = (Utilizator) this.utilizatorService.findOneDataBaseId(this.idUtilizator);
                boolean ok = this.utilizatorService.incarcaAnterioriiPrieteni(logat, LastOne);
                this.utilizatorService.addSmecher(logat);
                this.rearanjare(ok,logat);
                this.idButtonNextTabel.setVisible(true);
                if (!ok) {
                    this.idButtonPrevTabel.setVisible(false);
                }
            }
        else if(this.UltimulButonImportantApasat.equals("filter"))
        {
            Utilizator LastOne = this.tableId.getItems().get(0);
            Utilizator logat = (Utilizator) this.utilizatorService.findOneDataBaseId(this.idUtilizator);
            boolean ok = this.utilizatorService.incarcaPrevFiltrati(LastOne, logat,filtr);
            this.rearanjare(ok,logat);
            this.idButtonNextTabel.setVisible(true);
            if (!ok) {
                this.idButtonPrevTabel.setVisible(false);
            }
        }
        else{
            if(this.UltimulButonImportantApasat.equals("requests")){
                Utilizator LastOne = this.tableId.getItems().get(0);
                Utilizator logat = (Utilizator) this.utilizatorService.findOneDataBaseId(this.idUtilizator);
                boolean ok = this.utilizatorService.incarcaAnterioriiCereri(logat, LastOne);
                this.utilizatorService.addSmecher(logat);
                this.rearanjare(ok,logat);
                this.idButtonNextTabel.setVisible(true);
                if (!ok) {
                    this.idButtonPrevTabel.setVisible(false);
                }
            }
        }
    }



    public void nextEvent(MouseEvent mouseEvent) {
        if (this.UltimulButonImportantApasat.equals("clickSearch")) {
            Utilizator LastOne = this.tableId.getItems().get(this.tableId.getItems().size() - 1);
            Utilizator logat = (Utilizator) this.utilizatorService.findOneDataBaseId(this.idUtilizator);
            boolean ok = this.utilizatorService.incarcaNext(LastOne, logat);
            this.rearanjare(ok,logat);
            this.idButtonPrevTabel.setVisible(true);
            if (!ok) {
                this.idButtonNextTabel.setVisible(false);
            }
        }
        else if (this.UltimulButonImportantApasat.equals("friends")) {
            Utilizator LastOne = this.tableId.getItems().get(this.tableId.getItems().size() - 1);
            Utilizator logat = (Utilizator) this.utilizatorService.findOneDataBaseId(this.idUtilizator);
            boolean ok = this.utilizatorService.incarcaUrmatoriiPrieteni(logat, LastOne);
            this.utilizatorService.addSmecher(logat);
            this.rearanjare(ok, logat);
            this.idButtonPrevTabel.setVisible(true);
            if (!ok) {
                this.idButtonNextTabel.setVisible(false);
            }
        }
        else if(this.UltimulButonImportantApasat.equals("filter"))
        {
            Utilizator LastOne = this.tableId.getItems().get(this.tableId.getItems().size() - 1);
            Utilizator logat = (Utilizator) this.utilizatorService.findOneDataBaseId(this.idUtilizator);
            boolean ok = this.utilizatorService.incarcaNextFiltrati(LastOne, logat,filtr);
            this.rearanjare(ok,logat);
            this.idButtonPrevTabel.setVisible(true);
            if (!ok) {
                this.idButtonNextTabel.setVisible(false);
            }
        }
        else if(this.UltimulButonImportantApasat.equals("requests")){
                Utilizator LastOne = this.tableId.getItems().get(this.tableId.getItems().size() - 1);
                Utilizator logat = (Utilizator) this.utilizatorService.findOneDataBaseId(this.idUtilizator);
                boolean ok = this.utilizatorService.incarcaUrmatoriiCereri(logat, LastOne);
                this.utilizatorService.addSmecher(logat);
                this.rearanjare(ok,logat);
                this.idButtonPrevTabel.setVisible(true);
                if (!ok) {
                    this.idButtonNextTabel.setVisible(false);
                }

        }
    }

    private void rearanjare(boolean ok,Utilizator logat){
        this.prietenieService.DOOM();
        this.cereriPrietenieService.DOOM();
        this.utilizatorService.getAll().forEach(x -> {
            if (!x.getId().equals(idUtilizator)) {
                this.prietenieService.incarcaPrietenieDacaExista(logat, x);
                this.cereriPrietenieService.incarcaCerereDacaExista(logat, x);
            }
        });

        this.utilizatorService.reloadFriends();
        if(this.UltimulButonImportantApasat.equals("friends")||this.UltimulButonImportantApasat.equals("requests")||this.UltimulButonImportantApasat.equals("filter")) {
            this.loadModel(StreamSupport.stream(utilizatorService.getAll().spliterator(), false)
                    .filter(x -> x.getId() != idUtilizator)
                    .collect(Collectors.toList()));
        }
        else{
            this.loadModel(StreamSupport.stream(utilizatorService.getAll().spliterator(), false)
                    .collect(Collectors.toList()));
        }

    }

    public void filterEvent(KeyEvent keyEvent) {
        if(keyEvent.getCode().equals(KeyCode.ENTER)) {
            filtr = searchBarId.getText();
            UltimulButonImportantApasat = "filter";
            this.idButtonNextTabel.setVisible(false);
            this.idButtonPrevTabel.setVisible(false);
            this.incarcaStatusUtilizatoriFiltrati();
            this.loadModel(StreamSupport.stream(utilizatorService.getAll().spliterator(), false)
                    .filter(x -> x.getId() != idUtilizator)
                    .collect(Collectors.toList()));
        }
    }

    public void eventsEvent(MouseEvent mouseEvent) {
        try {
            Stage messenger = new Stage();
            FXMLLoader loader2 = new FXMLLoader();
            loader2.setLocation(getClass().getResource("/view/EventView.fxml"));
            AnchorPane root = loader2.load();
            Method method = loader2.getController().getClass().getMethod("init", UtilizatorService.class, EvenimenteService.class,Long.class);
            method.invoke(loader2.getController(), utilizatorService,evenimenteService,idUtilizator);
            Scene scene = new Scene(root,864.0 , 602.0);
            messenger.setScene(scene);
            messenger.setTitle("Events");
            messenger.setResizable(false);
            messenger.show();
        }
        catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }
}
