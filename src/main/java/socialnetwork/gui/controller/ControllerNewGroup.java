package socialnetwork.gui.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import socialnetwork.domain.Entity;
import socialnetwork.domain.Grup;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Utilizator;
import socialnetwork.service.*;

import java.io.File;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.stream.Collectors;

public class ControllerNewGroup implements Observer {
    private PrietenieService prietenieService;
    private ConversatieService conversatieService;
    private UtilizatorService utilizatorService;
    private MessageService messageService;
    private CereriPrietenieService cereriPrietenieService;
    private GrupService grupService;
    private GrupMessageService grupMessageService;
    private EvenimenteService evenimenteService;
    private Long idUtilizator;
    @FXML private Button  changePictureId;
    @FXML private ImageView pictureId;
    @FXML private TextField numeGrupTextField;
    @FXML private TableView<Utilizator> usersTableId;
    @FXML private TableColumn<Utilizator,ImageView> pictureTableColumn;
    @FXML private TableColumn<Utilizator,String> LastNameTableColumn;
    @FXML private TableColumn<Utilizator,String> FirstNameTableColumn;
    @FXML private Button CreazaGrupButtonId;
    ObservableList<Utilizator>model= FXCollections.observableArrayList();
    public void init(PrietenieService prietenieService, ConversatieService conversatieService, UtilizatorService utilizatorService, Long idUtilizator, MessageService messageService, GrupService grupService, GrupMessageService grupMessageService, EvenimenteService evenimenteService,CereriPrietenieService cereriPrietenieService) {
        this.prietenieService = prietenieService;
        this.conversatieService = conversatieService;
        this.utilizatorService = utilizatorService;
        this.idUtilizator = idUtilizator;
        this.messageService = messageService;
        this.grupService = grupService;
        this.cereriPrietenieService=cereriPrietenieService;
        this.grupMessageService = grupMessageService;
        this.evenimenteService = evenimenteService;
        this.CreazaGrupButtonId.setVisible(false);
        this.prietenieService.addObserver(this);
        this.prietenieService.addObserver(this);
        this.grupService.addObserver(this);
        this.utilizatorService.addObserver(this);
        this.usersTableId.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        Utilizator u=(Utilizator) this.utilizatorService.findOneDataBaseId(idUtilizator);
        this.utilizatorService.bagaTotiPrietenii(u);
        this.prietenieService.incarcaToatePrieteniile(u);
        this.conversatieService.incarcaToateConversatiile(u);
        this.prietenieService.getAll().forEach(x->{
            if(x.getId().getLeft().equals(u.getId())){
                u.addFriend(this.utilizatorService.findOne(x.getId().getRight()));
            }
            if(x.getId().getRight().equals(u.getId())){
                u.addFriend(this.utilizatorService.findOne(x.getId().getLeft()));
            }
        });
        this.loadModel(u.getFriends().stream()
                .map(x -> this.utilizatorService.findOne(x))
                .collect(Collectors.toList())
        );
        this.usersTableId.setItems(model);

        this.pictureTableColumn.setCellValueFactory(new PropertyValueFactory<Utilizator,ImageView>("imageView"));
        this.LastNameTableColumn.setCellValueFactory(new PropertyValueFactory<Utilizator,String>("lastName"));
        this.LastNameTableColumn.setCellFactory(new Callback<TableColumn<Utilizator, String>, TableCell<Utilizator, String>>() {
            @Override
            public TableCell<Utilizator, String> call(TableColumn<Utilizator, String> param) {
                return new TableCell<Utilizator, String>() {
                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);

                        if (isEmpty()) {
                            setText("");
                        } else {

                            setTextFill(Color.BLACK);
                            setFont(Font.font("Calibri", 20));
                            setAlignment(Pos.CENTER);
                            setTextAlignment(TextAlignment.CENTER);
                            setText(item);
                        }
                    }
                };
            }
        });
        this.FirstNameTableColumn.setCellValueFactory(new PropertyValueFactory<Utilizator,String>("firstName"));
        this.FirstNameTableColumn.setCellFactory(new Callback<TableColumn<Utilizator, String>, TableCell<Utilizator, String>>() {
            @Override
            public TableCell<Utilizator, String> call(TableColumn<Utilizator, String> param) {
                return new TableCell<Utilizator, String>() {
                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);

                        if (isEmpty()) {
                            setText("");
                        } else {

                            setTextFill(Color.BLACK);
                            setFont(Font.font("Calibri", 20));
                            setAlignment(Pos.CENTER);
                            setTextAlignment(TextAlignment.CENTER);
                            setText(item);
                        }
                    }
                };
            }
        });
    }
    private void loadModel(List<Utilizator> list ){
        model.setAll(list);

    }

    public void changePictureEvent(MouseEvent mouseEvent) {
        Stage stage = new Stage();
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                new FileChooser.ExtensionFilter("PNG", "*.png")
        );
        fileChooser.setTitle("Select profile picture");
        File file = fileChooser.showOpenDialog(stage);
        String imageUrl = null;
        if (file != null) {

            try {
                imageUrl = file.toURI().toURL().toExternalForm();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            Image image = new Image(imageUrl);
            this.pictureId.setImage(image);
        }

    }

    public void tableClickedEvent(MouseEvent mouseEvent) {
        String s=this.numeGrupTextField.getText().strip();
        if(!s.equals("")&&this.usersTableId.getSelectionModel().getSelectedItems().size()>=2){
            this.CreazaGrupButtonId.setVisible(true);
        }
        if(this.usersTableId.getSelectionModel().getSelectedItems().size()<2)
        {
            this.CreazaGrupButtonId.setVisible(false);
        }
    }

    public void CreateGroupEvent(MouseEvent mouseEvent) {
        ImageView imageView = new ImageView(new Image(this.pictureId.getImage().getUrl()));
        imageView.setFitWidth(40);
        imageView.setFitHeight(40);
        List<Long> members = new ArrayList<>();
        members.add(idUtilizator);
        this.usersTableId.getSelectionModel().getSelectedItems().forEach(x -> {
            members.add(x.getId());
        });
        Grup grup = new Grup(members, this.numeGrupTextField.getText().strip(), this.pictureId.getImage().getUrl(), imageView);
        grup.setId(null);
        this.grupService.addGrup(grup);
        try {
            Stage messenger = new Stage();
            FXMLLoader loader2 = new FXMLLoader();
            this.prietenieService.DOOM();
            this.cereriPrietenieService.DOOM();
            this.conversatieService.DOOM();
            this.utilizatorService.DOOM();
            loader2.setLocation(getClass().getResource("/view/MessengerView.fxml"));
            AnchorPane root = loader2.load();
            Method method = loader2.getController().getClass().getMethod("init", UtilizatorService.class, PrietenieService.class, MessageService.class, Long.class, ConversatieService.class, GrupService.class, GrupMessageService.class, CereriPrietenieService.class, EvenimenteService.class);
            method.invoke(loader2.getController(), utilizatorService, prietenieService, messageService, idUtilizator, conversatieService, grupService, grupMessageService, cereriPrietenieService, evenimenteService);
            Scene scene = new Scene(root, 1081.0, 761.0);
            messenger.setScene(scene);
            messenger.setTitle("Messenger");
            messenger.setResizable(false);
            messenger.show();
            Stage stage = (Stage) this.CreazaGrupButtonId.getScene().getWindow();
            stage.close();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }



    public void nameTypeEvent(KeyEvent keyEvent) {
        String s=this.numeGrupTextField.getText().strip();
        if(!s.equals("")&&this.usersTableId.getSelectionModel().getSelectedItems().size()>=2){
            this.CreazaGrupButtonId.setVisible(true);
        }
        if(s.equals("")){
            this.CreazaGrupButtonId.setVisible(false);
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        this.loadModel(this.utilizatorService.findOne(idUtilizator).getFriends().stream()
                .map(x -> this.utilizatorService.findOne(x))
                .collect(Collectors.toList())
        );
    }
}
