package socialnetwork.gui.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import socialnetwork.config.ApplicationContext;
import socialnetwork.domain.*;
import socialnetwork.domain.validators.*;
import socialnetwork.repository.Repository;
import socialnetwork.repository.database.*;
import socialnetwork.service.*;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ControllerLogin {

    @FXML
    private Button idLoginButton;
    @FXML
    private TextField idUsernameTextField;
    @FXML
    private PasswordField idPasswordField;
    @FXML
    private Button idSignUpButton;
    private PrietenieService prietenieService;
    private UtilizatorService utilizatorService;
    private MessageService messageService;
    private CereriPrietenieService cereriPrietenieService;
    private ConversatieService conversatieService;
    private GrupService grupService;
    private GrupMessageService grupMessageService;
    private EvenimenteService evenimenteService;

    public void init() {
        Validator<Utilizator> validator = new UtilizatorValidator();
        Validator<Message> validator2 = new MesajValidator();
        Repository<Long, Utilizator> utilizatorRepo = new UtilizatorDatabase(validator, ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork"));
        Validator<Prietenie> validator1 = new PrietenieValidator();
        Validator<CererePrietenie> validator3 = new CererePrietenieValidator();
        Validator<Conversatie>validator4=new ConversatieValidator();
        Validator<GrupMessage>validator6 =new GrupMessageValidator();
        Validator<Grup> validator5=new GrupValidator();
        Validator<Event>validator7=new EventValidator();
        AbstractDatabaseRepository<Long,GrupMessage>mesajGrupRepository=new GrupMessageDatabase(validator6,ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork"));
        AbstractDatabaseRepository<Long,Conversatie>conversatieRepository=new ConversatiiDatabase(validator4,ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork"));
        AbstractDatabaseRepository<Long,Grup>grupRepository=new GrupuriDatabase(validator5,ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork"));
        AbstractDatabaseRepository<Long, Message> messageRepo = new MesajeDatabase(validator2, ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork"));
        Repository<Tuple<Long, Long>, Prietenie> prietenieRepo = new PrieteniiDatabase(validator1, ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork"));
        Repository<Long,Event> eventRepository=new EvenimenteDatabase(validator7,ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork"));

        Repository<Tuple<Long, Long>, CererePrietenie> cererePrietenieRepo = new CereriPrietenieDatabase(validator3, ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork"));
        utilizatorService = new UtilizatorService(utilizatorRepo, prietenieRepo);
        prietenieService = new PrietenieService(prietenieRepo, utilizatorRepo);
        messageService = new MessageService(messageRepo, utilizatorRepo);
        conversatieService=new ConversatieService(conversatieRepository,utilizatorRepo);
        grupService=new GrupService(grupRepository,utilizatorRepo);
        grupMessageService=new GrupMessageService(grupRepository,utilizatorRepo,mesajGrupRepository);
        cereriPrietenieService = new CereriPrietenieService(prietenieRepo, cererePrietenieRepo, utilizatorRepo);
        evenimenteService=new EvenimenteService(utilizatorRepo,eventRepository);
    }

    public void handlerLoginButton(MouseEvent mouseEvent) {


        String username = idUsernameTextField.getText();
        String password = idPasswordField.getText();
        if (username.equals("") || password.equals("")) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Nu pot ramane campuri vide!", ButtonType.OK);
            alert.showAndWait();
        } else {
            try {
                Utilizator u = (Utilizator) this.utilizatorService.findOneDataBase(username);
                if (u == null) {
                    Alert alert = new Alert(Alert.AlertType.ERROR, "Acest username nu a fost inregistrat!", ButtonType.OK);
                    alert.showAndWait();
                } else {
                    int hash = password.hashCode();
                    if (String.valueOf(hash).equals(u.getHashing())) {
                        Stage utilizator = new Stage();
                        FXMLLoader loader2 = new FXMLLoader();
                        loader2.setLocation(getClass().getResource("/view/UtilizatorView.fxml"));
                        AnchorPane root = loader2.load();
                        Long id=u.getId();
                        Method method = loader2.getController().getClass().getMethod("init", PrietenieService.class, UtilizatorService.class, MessageService.class, CereriPrietenieService.class, Long.class, ConversatieService.class, GrupService.class, GrupMessageService.class,EvenimenteService.class);
                        method.invoke(loader2.getController(), prietenieService, utilizatorService, messageService, cereriPrietenieService, id, conversatieService, grupService, grupMessageService,evenimenteService);

                        Scene scene = new Scene(root, 862.0, 777.0);
                        utilizator.setScene(scene);
                        utilizator.setTitle("Social Network");
                        utilizator.setResizable(false);
                        utilizator.show();
                        Stage stage = (Stage) idLoginButton.getScene().getWindow();
                        // do what you have to do
                        stage.close();
                    } else {
                        Alert alert = new Alert(Alert.AlertType.ERROR, "Parola Invalida!", ButtonType.OK);
                        alert.showAndWait();
                    }
                }
            } catch (IOException | NoSuchMethodException|InvocationTargetException|IllegalAccessException ex) {
                System.out.println(ex.getMessage());
            }

        }
    }

    public void signUpClickedEvent(MouseEvent mouseEvent) {
        try {
            Stage utilizator = new Stage();
            FXMLLoader loader2 = new FXMLLoader();
            loader2.setLocation(getClass().getResource("/view/SignUpView.fxml"));
            AnchorPane root = loader2.load();
            Method method = loader2.getController().getClass().getMethod("init",UtilizatorService.class);
            method.invoke(loader2.getController(),utilizatorService);

            Scene scene = new Scene(root, 600, 400);
            utilizator.setScene(scene);
            utilizator.setTitle("Sign Up");
            utilizator.setResizable(false);
            utilizator.show();
        }
        catch(IOException|NoSuchMethodException|IllegalAccessException|InvocationTargetException ex){
            System.out.println(ex.getMessage());
        }
    }
}
