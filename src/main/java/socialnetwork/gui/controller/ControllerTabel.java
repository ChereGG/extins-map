package socialnetwork.gui.controller;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.util.Callback;
import socialnetwork.domain.Conversatie;
import socialnetwork.domain.Grup;
import socialnetwork.domain.Utilizator;
import socialnetwork.service.*;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.lang.reflect.Method;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ControllerTabel {
    private PrietenieService prietenieService;
    private ConversatieService conversatieService;
    private UtilizatorService utilizatorService;
    private MessageService messageService;
    private CereriPrietenieService cereriPrietenieService;
    private GrupService grupService;
    private GrupMessageService grupMessageService;
    private EvenimenteService evenimenteService;
    Long idUtilizator;
    @FXML
    private TableView<Utilizator> idFriendsTable;
    @FXML
    private TableView<Conversatie> idMessageTable;
    @FXML
    private TableColumn<Utilizator, ImageView> pictureColumnId;
    @FXML
    private TableColumn<Utilizator, String> numeColumnId;
    @FXML
    private TableColumn<Utilizator, String> prenumeColumnId;
    @FXML
    private TableColumn<Conversatie, String> fromColumnId;
    @FXML
    private TableColumn<Conversatie, String> dateColumnId;
    @FXML
    private TableColumn<Conversatie, String> mesajColumnId;

    @FXML
    private DatePicker beginDateId;
    @FXML
    private DatePicker endDateId;
    @FXML
    private Button exportButtonId;
    @FXML
    private Button idButtonExit;
    private ObservableList<Utilizator> listaPrieteni = FXCollections.observableArrayList();
    private ObservableList<Conversatie> listaMesaje = FXCollections.observableArrayList();

    public void init(PrietenieService prietenieService, ConversatieService conversatieService, UtilizatorService utilizatorService, Long idUtilizator, MessageService messageService, GrupService grupService, GrupMessageService grupMessageService, EvenimenteService evenimenteService,CereriPrietenieService cereriPrietenieService) {
        this.prietenieService = prietenieService;
        this.conversatieService = conversatieService;
        this.utilizatorService = utilizatorService;
        this.idUtilizator = idUtilizator;
        this.messageService = messageService;
        this.grupService = grupService;
        this.cereriPrietenieService=cereriPrietenieService;
        this.grupMessageService = grupMessageService;
        this.evenimenteService = evenimenteService;
        this.beginDateId.setValue(LocalDate.now());
        this.endDateId.setValue(LocalDate.now());
        this.beginDateId.setEditable(false);
        this.endDateId.setEditable(false);
        Utilizator u=(Utilizator) this.utilizatorService.findOneDataBaseId(idUtilizator);
        this.utilizatorService.bagaTotiPrietenii(u);
        this.prietenieService.incarcaToatePrieteniile(u);
        this.conversatieService.incarcaToateConversatiile(u);
        this.prietenieService.getAll().forEach(x->{
           if(x.getId().getLeft().equals(u.getId())){
               u.addFriend(this.utilizatorService.findOne(x.getId().getRight()));
           }
           if(x.getId().getRight().equals(u.getId())){
               u.addFriend(this.utilizatorService.findOne(x.getId().getLeft()));
           }
        });

        this.loadModelUsers(u.getFriends().stream()
                .map(x -> this.utilizatorService.findOne(x))
                .collect(Collectors.toList())
        );
        this.idFriendsTable.setItems(listaPrieteni);
        this.idMessageTable.setItems(listaMesaje);
        this.pictureColumnId.setCellValueFactory(new PropertyValueFactory<Utilizator, javafx.scene.image.ImageView>("imageView"));
        this.numeColumnId.setCellValueFactory(new PropertyValueFactory<Utilizator, String>("lastName"));
        this.numeColumnId.setCellFactory(new Callback<TableColumn<Utilizator, String>, TableCell<Utilizator, String>>() {
            @Override
            public TableCell<Utilizator, String> call(TableColumn<Utilizator, String> param) {
                return new TableCell<Utilizator, String>() {
                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);

                        if (isEmpty()) {
                            setText("");
                        } else {

                            setTextFill(Color.BLACK);
                            setFont(Font.font("Calibri", 20));
                            setAlignment(Pos.CENTER);
                            setTextAlignment(TextAlignment.CENTER);
                            setText(item);
                        }
                    }
                };
            }
        });
        this.prenumeColumnId.setCellValueFactory(new PropertyValueFactory<Utilizator, String>("firstName"));
        this.prenumeColumnId.setCellFactory(new Callback<TableColumn<Utilizator, String>, TableCell<Utilizator, String>>() {
            @Override
            public TableCell<Utilizator, String> call(TableColumn<Utilizator, String> param) {
                return new TableCell<Utilizator, String>() {
                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);

                        if (isEmpty()) {
                            setText("");
                        } else {

                            setTextFill(Color.BLACK);
                            setFont(Font.font("Calibri", 20));
                            setAlignment(Pos.CENTER);
                            setTextAlignment(TextAlignment.CENTER);
                            setText(item);
                        }
                    }
                };
            }
        });
        this.fromColumnId.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Conversatie, String>, ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Conversatie, String> param) {
                if (param != null) {
                    return new SimpleStringProperty(utilizatorService.findOne(param.getValue().getFrom()).getFirstName() + " " + utilizatorService.findOne(param.getValue().getFrom()).getLastName());
                }
                return new SimpleStringProperty("--empty--");
            }
        });

        this.dateColumnId.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Conversatie, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Conversatie, String> param) {
                if (param != null) {
                    return new SimpleStringProperty(param.getValue().getDate().toLocalDate().toString());
                }
                return new SimpleStringProperty("--empty--");
            }
        });

        this.mesajColumnId.setCellValueFactory(new PropertyValueFactory<Conversatie, String>("message"));

    }


    private void loadModelUsers(List<Utilizator> list) {
        listaPrieteni.setAll(list);
    }

    private void loadModelConversatii(List<Conversatie> list) {
        listaMesaje.setAll(list);
    }

    public void friendSelectedEvent(MouseEvent mouseEvent) {
        Utilizator utilizator = idFriendsTable.getSelectionModel().getSelectedItem();
        LocalDate date = this.beginDateId.getValue();
        LocalDate date1 = this.endDateId.getValue();
        List<Conversatie> conversatie = conversatieService.toateMesajeleIntervalData(idUtilizator, date, date1).stream()
                .filter(x -> x.getFrom().equals(utilizator.getId()))
                .collect(Collectors.toList());
        this.loadModelConversatii(conversatie);
    }

    public void exportEvent(MouseEvent mouseEvent) throws FileNotFoundException, DocumentException {
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream("RaportTabel.pdf"));

        document.open();

        Paragraph paragraph = new Paragraph();
        com.itextpdf.text.Font font = FontFactory.getFont(FontFactory.COURIER_BOLD, 20);
        Chunk chunk = new Chunk("Raport Mesaje", font);
        Paragraph paragraph1 = new Paragraph(chunk);
        paragraph1.setAlignment(Element.ALIGN_CENTER);
        paragraph.add(paragraph1);
        paragraph.add(new Paragraph(" "));
        paragraph.add(new Paragraph(" "));
        paragraph.add(new Paragraph(" "));
        paragraph.add(new Paragraph(" "));
        Utilizator u = (Utilizator)this.utilizatorService.findOneDataBaseId(idUtilizator);
        paragraph.add(new Paragraph("Raportul pentru utilizatorul: " +
                u.getFirstName() +
                " " +
                u.getLastName() +
                " privind mesajele primite de la utilizatorul selectat ordonate cronologic incepand din data: " +
                this.beginDateId.getValue().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")) +
                " pana la data: " +
                this.endDateId.getValue().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")) +
                "  Va multumim pentru incredere!"));

        PdfPTable table = new PdfPTable(3);
        addTableHeader(table);
        addRows(table);
        paragraph.add(new Paragraph(" "));
        paragraph.add(new Paragraph(" "));
        paragraph.add(new Paragraph(" "));
        paragraph.add(new Paragraph(" "));
        document.add(paragraph);

        document.add(table);
        document.close();
        Alert alert = new Alert(Alert.AlertType.INFORMATION, "Raport salvat!", ButtonType.OK);
        alert.showAndWait();
    }

    private void addTableHeader(PdfPTable table) {
        Stream.of("nume", "data", "mesaj")
                .forEach(columnTitle -> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    header.setBorderWidth(2);
                    header.setPhrase(new Phrase(columnTitle));
                    table.addCell(header);
                });
    }

    private void addRows(PdfPTable table) {
        idMessageTable.getItems().forEach(x -> {
            table.addCell(utilizatorService.findOne(x.getFrom()).getLastName() + " " + utilizatorService.findOne(x.getFrom()).getFirstName());
            table.addCell(x.getDate().toLocalDate().toString());
            table.addCell(x.getMessage());
        });

    }

    public void ExitEvent(MouseEvent mouseEvent) {
        try {
            Stage utilizator = new Stage();
            FXMLLoader loader2 = new FXMLLoader();
            loader2.setLocation(getClass().getResource("/view/UtilizatorView.fxml"));
            AnchorPane root = loader2.load();
            this.prietenieService.DOOM();
            this.cereriPrietenieService.DOOM();
            this.conversatieService.DOOM();
            this.utilizatorService.DOOM();
            Method method = loader2.getController().getClass().getMethod("init", PrietenieService.class, UtilizatorService.class, MessageService.class, CereriPrietenieService.class, Long.class, ConversatieService.class, GrupService.class, GrupMessageService.class, EvenimenteService.class);
            method.invoke(loader2.getController(), prietenieService, utilizatorService, messageService, cereriPrietenieService, idUtilizator, conversatieService, grupService, grupMessageService, evenimenteService);
            Scene scene = new Scene(root, 862.0, 777.0);
            utilizator.setScene(scene);
            utilizator.setTitle("Social Network");
            utilizator.setResizable(false);
            utilizator.show();
            Stage stage = (Stage) idButtonExit.getScene().getWindow();
            stage.close();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}

