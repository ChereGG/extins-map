package socialnetwork.domain;

import javafx.scene.image.ImageView;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Objects;

public class Message extends Entity<Long> {
    protected Long from;
    protected ArrayList<Long>to;
    protected String message;
    protected LocalDateTime date;
    protected  Long repliedMessage;
    protected Long reply;
    protected String groupName;
    protected String picturePath;
    protected ImageView imageView;

    public Message(Long from, ArrayList<Long> to, String message, LocalDateTime date) {
        this.from = from;
        this.to = to;
        this.message = message;
        this.date = date;
        this.repliedMessage =null;
        this.reply=null;
        this.groupName=null;
        this.imageView=null;
        this.picturePath=null;
    }

    public Message(Long from, ArrayList<Long> to, String message, LocalDateTime date, String groupName, ImageView imageView,String picturePath) {
        this.from = from;
        this.to = to;
        this.message = message;
        this.date = date;
        this.groupName = groupName;
        this.imageView = imageView;
        this.picturePath=picturePath;
        this.repliedMessage =null;
        this.reply=null;
    }

    public String getPicturePath() {
        return picturePath;
    }

    public void setPicturePath(String picturePath) {
        this.picturePath = picturePath;
    }

    public String getGroupName() {
        return groupName;
    }

    public ImageView getImageView() {
        return imageView;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public void setImageView(ImageView imageView) {
        this.imageView = imageView;
    }

    public void setRepliedMessage(Long repliedMessage) {
        this.repliedMessage = repliedMessage;
    }

    public Long getReply() {
        return reply;
    }

    public void setReply(Long reply) {
        this.reply = reply;
    }

    public Long getRepliedMessage() {
        return repliedMessage;
    }



    public Long getFrom() {
        return from;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message1 = (Message) o;
        return Objects.equals(from, message1.from) &&
                Objects.equals(to, message1.to) &&
                Objects.equals(message, message1.message) &&
                Objects.equals(date, message1.date) &&
                Objects.equals(repliedMessage, message1.repliedMessage) &&
                Objects.equals(reply, message1.reply);
    }

    @Override
    public int hashCode() {
        return Objects.hash(from, to, message, date, repliedMessage, reply);
    }

    public ArrayList<Long> getTo() {
        return to;
    }

    public String getMessage() {
        return message;
    }

    public LocalDateTime getDate() {
        return date;
    }
}
