package socialnetwork.domain.validators;

import socialnetwork.domain.CererePrietenie;
import socialnetwork.domain.Prietenie;

public class CererePrietenieValidator implements Validator<CererePrietenie> {
    @Override
    public void validate(CererePrietenie entity) throws ValidationException {
        if(entity.getId().getLeft()<0 || entity.getId().getRight()<0)
            throw new ValidationException("Both ids must be positive numbers!");

    }
}
