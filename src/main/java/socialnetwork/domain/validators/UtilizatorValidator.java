package socialnetwork.domain.validators;

import socialnetwork.domain.Utilizator;

public class UtilizatorValidator implements Validator<Utilizator> {
    @Override
    public void validate(Utilizator entity) throws ValidationException {
        String errors = "";
        if(entity.getFirstName().equals(""))
            errors += "First name must contain at least 1 character!\n";
        if(entity.getLastName().equals(""))
            errors += "Last name must contain at least 1 character!\n";
        if(! entity.getFirstName().matches("^[A-Z][a-z]+$"))
        {
            errors += "First name must not contain special characters and must start with a capital letter\n";
        }
        if(! entity.getLastName().matches("^[A-Z][a-z]+$"))
        {
            errors += "First name must not contain special characters and must start with a capital letter\n";
        }
        if(!errors.equals(""))
            throw new ValidationException(errors);
    }
}
