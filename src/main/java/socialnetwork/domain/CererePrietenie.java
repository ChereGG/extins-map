package socialnetwork.domain;

import java.util.Objects;

public class CererePrietenie extends Entity<Tuple<Long,Long>> {
    private Status status;

    public CererePrietenie() {
        status = Status.PENDING;
    }

    public CererePrietenie(Status status) {
        this.status = status;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }


}
