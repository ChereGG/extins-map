package socialnetwork.domain;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class Conversatie extends Entity<Long> {
    protected Long from;
    protected Long to;
    protected String message;
    protected LocalDateTime date;

    public Conversatie(Long from, Long to, String message, LocalDateTime date) {
        this.from = from;
        this.to = to;
        this.message = message;
        this.date = date;
    }

    public Long getFrom() {
        return from;
    }

    public Long getTo() {
        return to;
    }

    public String getMessage() {
        return message;
    }

    public LocalDateTime getDate() {
        return date;
    }

}
