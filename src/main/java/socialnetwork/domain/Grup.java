package socialnetwork.domain;

import javafx.scene.image.ImageView;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Grup extends Entity<Long> {
    protected List<Long> members;
    protected String groupName;
    protected String picturePath;
    protected ImageView imageView;

    public Grup(List<Long> members, String groupName, String picturePath, ImageView imageView) {
        this.members = members;

        this.groupName = groupName;
        this.picturePath = picturePath;
        this.imageView = imageView;
    }

    public List<Long> getMembers() {
        return members;
    }


    public String getGroupName() {
        return groupName;
    }

    public String getPicturePath() {
        return picturePath;
    }

    public ImageView getImageView() {
        return imageView;
    }
}
