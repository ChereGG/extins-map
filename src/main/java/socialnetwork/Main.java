package socialnetwork;
import javafx.scene.chart.Chart;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Button;
import org.apache.pdfbox.pdmodel.PDDocument;
import socialnetwork.config.ApplicationContext;
import socialnetwork.domain.*;
import socialnetwork.domain.validators.*;
import socialnetwork.gui.LoginGui;
import socialnetwork.repository.Repository;
import socialnetwork.repository.database.*;
import socialnetwork.repository.file.PrieteniiFile;
import socialnetwork.repository.file.UtilizatorFile;
import socialnetwork.service.*;
import socialnetwork.ui.Ui;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) throws IOException, SQLException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
   /*   PrietenieService prietenieService;
   UtilizatorService utilizatorService;
     MessageService messageService;
  CereriPrietenieService cereriPrietenieService;
  ConversatieService conversatieService;
     GrupService grupService;
   GrupMessageService grupMessageService;
        Validator<Utilizator> validator = new UtilizatorValidator();
        Validator<Message> validator2 = new MesajValidator();
        Repository<Long, Utilizator> utilizatorRepo = new UtilizatorDatabase(validator, ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork"));
        Validator<Prietenie> validator1 = new PrietenieValidator();
        Validator<CererePrietenie> validator3 = new CererePrietenieValidator();
        Validator<Conversatie>validator4=new ConversatieValidator();
        Validator<GrupMessage>validator6 =new GrupMessageValidator();
        Validator<Grup> validator5=new GrupValidator();
        AbstractDatabaseRepository<Long,GrupMessage>mesajGrupRepository=new GrupMessageDatabase(validator6,ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork"));
        AbstractDatabaseRepository<Long,Conversatie>conversatieRepository=new ConversatiiDatabase(validator4,ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork"));
        AbstractDatabaseRepository<Long,Grup>grupRepository=new GrupuriDatabase(validator5,ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork"));
        AbstractDatabaseRepository<Long, Message> messageRepo = new MesajeDatabase(validator2, ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork"));
        Repository<Tuple<Long, Long>, Prietenie> prietenieRepo = new PrieteniiDatabase(validator1, ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork"));
        Repository<Tuple<Long, Long>, CererePrietenie> cererePrietenieRepo = new CereriPrietenieDatabase(validator3, ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork"));
        utilizatorService = new UtilizatorService(utilizatorRepo, prietenieRepo);
        prietenieService = new PrietenieService(prietenieRepo, utilizatorRepo);
        messageService = new MessageService(messageRepo, utilizatorRepo);
        conversatieService=new ConversatieService(conversatieRepository,utilizatorRepo);
        grupService=new GrupService(grupRepository,utilizatorRepo);
        grupMessageService=new GrupMessageService(grupRepository,utilizatorRepo,mesajGrupRepository);
        cereriPrietenieService = new CereriPrietenieService(prietenieRepo, cererePrietenieRepo, utilizatorRepo);
        System.out.println(utilizatorRepo.findAll());
        Method method=utilizatorRepo.getClass().getMethod("incarcaNext", Utilizator.class);
        method.invoke(utilizatorRepo,utilizatorRepo.findOne(Long.parseLong("21")));
        System.out.println(utilizatorRepo.findAll());*/
       LoginGui.main(args);


    }
}


