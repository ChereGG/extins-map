package socialnetwork.ui;
import socialnetwork.domain.*;
import socialnetwork.domain.validators.RepoException;
import socialnetwork.domain.validators.ServiceException;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.service.CereriPrietenieService;
import socialnetwork.service.MessageService;
import socialnetwork.service.PrietenieService;
import socialnetwork.service.UtilizatorService;
import java.io.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicReference;

public class UiUtilizator {
    private final MessageService messageService;
    private final UtilizatorService utilizatorService;

    private CereriPrietenieService cereriPrietenieService;
    private PrietenieService prietenieService;
    Long utilizatorId;
    Long incrementator;

    public UiUtilizator(MessageService messageService, UtilizatorService utilizatorService, Long utilizatorId, CereriPrietenieService cereriPrietenieService, PrietenieService prietenieService) {
        this.messageService = messageService;
        this.utilizatorService = utilizatorService;
        this.utilizatorId = utilizatorId;
        this.cereriPrietenieService = cereriPrietenieService;
        this.prietenieService = prietenieService;
        try {
            BufferedReader br = new BufferedReader(new FileReader("data/increaser.txt"));
            String line = br.readLine();
            this.incrementator = Long.parseLong(line);
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void writeIncreaser(){
        try {
            BufferedWriter bW = new BufferedWriter(new FileWriter("data/increaser.txt"));
            bW.write(this.incrementator.toString()+"\n");
            bW.close();
           //bW.newLine();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void  run() throws IOException {
        BufferedReader reader=new BufferedReader(new InputStreamReader(System.in));
        String meniu=
                "1.Mesaje private noi\n"+
                "2.Afisare mesaje cu utilizator dat\n"+
                "3.Raspunde unui mesaj\n"+
                "4.Trimite un mesaj\n"+
                "5.Conversatiile de grup\n"+
                "6.Raspunde unui grup\n"+
                "7.Cereri de prietenie\n"+
                "8.Trimite o cerere de prietenie\n"+
                "9.Raspunde unei cereri de prietenie\n"+
                "10.Delogare\n";
        boolean ok=true;
        while(ok) {
            try {
                System.out.println(meniu);
                String command = reader.readLine();
                Long id;
                String Text;
                switch (command) {
                    case "1":
                        ArrayList<Message> mesaje = this.messageService.mesajeLaCareNuADatReply(this.utilizatorId);
                        mesaje.forEach(x -> {
                            String s = "";
                            Utilizator from = this.utilizatorService.findOne(x.getFrom());
                            s += "ID: " + x.getId() + " Text: " + x.getMessage() + " From: " + from.getFirstName() + " " + from.getLastName() + "\n";
                            System.out.println(s);
                        });
                        break;
                    case "2":
                        System.out.println("Dati id");
                        try {
                            id = Long.parseLong(reader.readLine());
                        }
                        catch (IllegalArgumentException illegalArgumentException) {
                            System.out.println("Id must be a positive number");
                            break;
                        }
                        ArrayList<ArrayList<Message>> toateConversatiile=this.messageService.toateConversatiile(this.utilizatorId,id);
                        toateConversatiile.forEach(x->{
                            for(int i=x.size()-1;i>=0;i--){
                                String s = "";
                                Utilizator from = this.utilizatorService.findOne(x.get(i).getFrom());
                                s += "ID: " + x.get(i).getId() + " Text: " + x.get(i).getMessage() + " From: " + from.getFirstName() + " " + from.getLastName() + "\n";
                                System.out.println(s);
                            }

                            if(x.size() != 0) {
                                System.out.println("-------------------------------------------------------------------\n");
                            }
                        });
                        break;
                    case "3":
                        mesaje = this.messageService.mesajeLaCareNuADatReply(this.utilizatorId);
                        System.out.println("Dati id-ul mesajului");
                        try {
                            id = Long.parseLong(reader.readLine());
                        }
                        catch (IllegalArgumentException illegalArgumentException) {
                            System.out.println("Id must be a positive number");
                            break;
                        }
                        if(!messageService.existaMesaj(mesaje,id))
                        {
                            System.out.println("Nu exista niciun mesaj cu acest id!");
                            break;
                        }
                        System.out.println("Introduceti textul mesajului:");
                        Text=reader.readLine();

                        ArrayList<Long>to=new ArrayList<>();
                        to.add(this.messageService.findOne(id).getFrom());
                        Message message=new Message(utilizatorId,to,Text, LocalDateTime.now());
                        this.incrementator++;
                        message.setId(incrementator);
                        message.setRepliedMessage(id);
                        this.messageService.addMessage(message);
                        this.writeIncreaser();

                        break;
                    case "4":
                        System.out.println("Introduceti textul mesajului:");
                        Text=reader.readLine();
                        System.out.println("Adaugati id-urile utilizatorilor doriti ca destinatari separate prin spatiu\n");
                        String input=reader.readLine();
                        String[] destinatari=input.split(" ");
                        ArrayList<Long>to2=new ArrayList<>();
                        for(String ids:destinatari){
                            to2.add(Long.parseLong(ids));
                        }

                        System.out.println("1.Creeaza grup\n" + "2.Trimite mesajul individual fiecarui participant");
                        String choiceut = reader.readLine();
                        if(choiceut.equals("1"))
                        {
                            Message message2=new Message(utilizatorId,to2,Text,LocalDateTime.now());
                            this.incrementator++;
                            message2.setId(incrementator);
                            this.messageService.addMessage(message2);
                            this.writeIncreaser();
                        }
                        else if(choiceut.equals("2"))
                        {
                            to2.forEach(x-> {
                                ArrayList<Long> too = new ArrayList<>();
                                too.add(x);
                                Message message2=new Message(utilizatorId,too,Text,LocalDateTime.now());
                                this.incrementator++;
                                message2.setId(incrementator);
                                this.messageService.addMessage(message2);
                                this.writeIncreaser();
                            });
                        }
                        else{
                            System.out.println("Comanda invalida!");
                        }
                        break;
                    case"5":
                        ArrayList<ArrayList<Message>> toateConversatiileGrup=this.messageService.toateConversatiileGrup(this.utilizatorId);
                        toateConversatiileGrup.forEach(x->{
                            AtomicReference<String> grup = new AtomicReference<>("In group with:");
                            x.get(0).getTo().forEach(y->{
                                grup.set(grup.get() + " " + this.utilizatorService.findOne(y).getFirstName() + " " + this.utilizatorService.findOne(y).getLastName() + " ,");
                            });
                            for(int i=x.size()-1;i>=0;i--){
                                String s = "";
                                Utilizator from = this.utilizatorService.findOne(x.get(i).getFrom());
                                s += "ID: " + x.get(i).getId() + " Text: " + x.get(i).getMessage() + " From: " + from.getFirstName() + " " + from.getLastName() + "\n";
                                System.out.println(s);
                            }
                            System.out.println(grup.get());
                            if(x.size() != 0) {
                                System.out.println("-------------------------------------------------------------------\n");
                            }
                        });
                        break;
                    case"6":
                        mesaje = this.messageService.mesajeLaCareNuADatReplyGrup(this.utilizatorId);
                        System.out.println("Dati id-ul mesajului");
                        try {
                            id = Long.parseLong(reader.readLine());
                        }
                        catch (IllegalArgumentException illegalArgumentException) {
                            System.out.println("Id must be a positive number");
                            break;
                        }
                        if(!this.messageService.existaMesaj(mesaje,id))
                        {
                            System.out.println("Mesajul nu exista!");
                            break;
                        }
                        System.out.println("Introduceti textul mesajului:");
                        Text=reader.readLine();
                        ArrayList<Long>to3=(ArrayList<Long>)this.messageService.findOne(id).getTo().clone();
                        to3.remove(utilizatorId);
                        to3.add(this.messageService.findOne(id).getFrom());
                        Message message3=new Message(utilizatorId,to3,Text, LocalDateTime.now());
                        this.incrementator++;
                        message3.setId(incrementator);
                        message3.setRepliedMessage(id);
                        this.messageService.addMessage(message3);
                        this.writeIncreaser();
                        break;
                    case"7":
                        ArrayList<CererePrietenie> arrayList = this.cereriPrietenieService.cereriDePrietenie(utilizatorId);
                        arrayList.forEach(x->{
                            Utilizator utilizator = utilizatorService.findOne(x.getId().getLeft());
                            if(utilizator != null)
                            {
                                System.out.println("Id: " + utilizator.getId() + " Nume: " + utilizator.getFirstName() + " " + utilizator.getLastName());
                            }
                        });
                        break;
                    case"8":
                        System.out.println("Dati id-ul utilizatorului caruia doresti sa ii trimiti cererea de prietenie");
                        try {
                            id = Long.parseLong(reader.readLine());
                        }
                        catch (IllegalArgumentException illegalArgumentException) {
                            System.out.println("Id must be a positive number");
                            break;
                        }
                        CererePrietenie cererePrietenie = new CererePrietenie();
                        cererePrietenie.setId(new Tuple<Long, Long>(utilizatorId,id));
                        cereriPrietenieService.addCererePrietenie(cererePrietenie);
                        break;
                    case"9":
                        System.out.println("Dati id-ul utilizatorului caruia doresti sa ii raspunzi la cererea de prietenie");
                        try {
                            id = Long.parseLong(reader.readLine());
                        }
                        catch (IllegalArgumentException illegalArgumentException) {
                            System.out.println("Id must be a positive number");
                            break;
                        }
                        if(!this.utilizatorService.existaUtilizator(id))
                        {
                            System.out.println("Utilizatorul nu mai exista!");
                            break;
                        }
                        System.out.println("1. Accepta cerere\n 2.Respinge cerere");
                        String choice = reader.readLine();
                        CererePrietenie cererePrietenie1;
                        if(choice.equals("1"))
                        {
                            cererePrietenie1 = new CererePrietenie(Status.APPROVED);
                            cererePrietenie1.setId(new Tuple<>(id,utilizatorId));
                            cereriPrietenieService.updateCererePrietenie(cererePrietenie1);
                            Prietenie prietenie = new Prietenie();
                            prietenie.setId(new Tuple<>(id,utilizatorId));
                            this.prietenieService.addPrietenie(prietenie);

                        }
                        else if(choice.equals("2"))
                        {
                            cererePrietenie1 = new CererePrietenie(Status.REJECTED);
                            cererePrietenie1.setId(new Tuple<>(id,utilizatorId));
                            cereriPrietenieService.updateCererePrietenie(cererePrietenie1);
                        }
                        else{
                            System.out.println("Comanda incorecta");
                        }
                        break;
                    case"10":
                        ok=false;
                        break;
                }

            }
            catch (ValidationException | RepoException | ServiceException exception){
                System.out.println(exception.getMessage());
            }
        }


    }
}
