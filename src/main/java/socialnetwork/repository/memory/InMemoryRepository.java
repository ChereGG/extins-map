package socialnetwork.repository.memory;

import socialnetwork.domain.Entity;
import socialnetwork.domain.validators.RepoException;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;

import java.util.*;

public class InMemoryRepository<ID, E extends Entity<ID>> implements Repository<ID,E> {

    protected Validator<E> validator;
    protected Map<ID, E> entities;

    public InMemoryRepository(Validator<E> validator) {
        this.validator = validator;
        entities = new LinkedHashMap<>();
    }


    @Override
    public E findOne(ID id) {
        if (id == null)
            throw new IllegalArgumentException("id must be not null");
        return entities.get(id);
    }

    @Override
    public Iterable<E> findAll() {
        return entities.values();
    }

    @Override
    public E save(E entity) {
        if (entity == null)
            throw new IllegalArgumentException("entity must be not null");
        validator.validate(entity);
        if (entities.containsKey(entity.getId()))
            throw new RepoException("The entity with the given id already exists!");
        return entities.put(entity.getId(), entity);

    }

    @Override
    public E delete(ID id) {
        if (id == null)
            throw new IllegalArgumentException("Id must be not null!");
        if (!entities.containsKey(id)) {
            throw new RepoException("Invalid firendship id!");
        }
        ArrayList<E> en = new ArrayList<>();
        entities.values().forEach(entity -> {
            if (entity.getId().equals(id)) {
                en.add(entity);

            }
        });
        if (en.size() == 0)
            return null;
        if (en.get(0) != null) {
            entities.remove(id);
            return en.get(0);
        }
        return null;
    }

    @Override
    public E update(E entity) {

        if (entity == null)
            throw new IllegalArgumentException("Entity must be not null!");
        validator.validate(entity);
        if (!entities.containsKey(entity.getId())) {
            throw new RepoException("An entity with the new entity's id does not exist!");
        }
        return entities.put(entity.getId(), entity);

    }

    public int numberOfEntities(){
        return this.entities.size();
    }

    @Override
    public void DOOM() {
        this.entities.clear();
    }

    @Override
    public void addSmehcer(E e) {
        this.entities.put(e.getId(),e);
    }


}


