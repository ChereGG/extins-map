package socialnetwork.repository.database;

import socialnetwork.domain.Conversatie;
import socialnetwork.domain.Message;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.RepoException;
import socialnetwork.domain.validators.Validator;
import sun.nio.ch.Util;

import java.sql.*;
import java.time.LocalDateTime;

public class ConversatiiDatabase extends AbstractDatabaseRepository<Long, Conversatie> {
    public ConversatiiDatabase(Validator<Conversatie> validator, String url) {
        super(validator, url);
    }

    @Override
    public void loadData() {
        try {
            Connection connection = DriverManager.getConnection(url);

            PreparedStatement statement = connection.prepareStatement("SELECT * FROM [socialNetwork].Conversatii");
            ResultSet resultSet = statement.executeQuery();
            {
                while (resultSet.next()) {
                    Conversatie message = this.extractEntity(resultSet);
                    super.superSave(message);
                }
            }
            connection.close();
        } catch (SQLException exception) {
            throw new RepoException("The database connection failed");
        }
    }

    @Override
    public Conversatie extractEntity(ResultSet resultSet) {
        try {
            Long id = resultSet.getLong("idConversatie");
            Long to = resultSet.getLong("toConversatie");
            Long from = resultSet.getLong("fromConversatie");
            String mesaj = resultSet.getString("mesajConversatie");
            String date = resultSet.getString("dataConversatie");
            LocalDateTime mesajData = LocalDateTime.parse(date);
            Conversatie conversatie = new Conversatie(from, to, mesaj, mesajData);
            conversatie.setId(id);
            return conversatie;
        } catch (SQLException exception) {
            throw new RepoException("Could not extract entity from table. Check if the fields' values are valid!");
        }
    }

    @Override
    public void writeToDatabase(Conversatie conversatie) {
        try {
            Connection connection = DriverManager.getConnection(url);
            Statement statement = connection.createStatement();
            statement.executeUpdate("INSERT INTO [socialNetwork].Conversatii" +
                    " VALUES (" + conversatie.getTo().toString() + "," + conversatie.getFrom() + ",'" +
                    conversatie.getMessage() + "','" + conversatie.getDate().toString() + "')", Statement.RETURN_GENERATED_KEYS);
            ResultSet keys = statement.getGeneratedKeys();
            Long key = null;
            if (keys.next()) {
                key = keys.getLong(1);
            }
            conversatie.setId(key);
        } catch (SQLException exception) {
            throw new RepoException("Could not write to table!");
        }
    }

    @Override
    public void emptyTable() {
        try {
            Connection connection = DriverManager.getConnection(url);
            Statement statement = connection.createStatement();

            statement.executeUpdate("DELETE FROM [socialNetwork].Conversatii");
        } catch (SQLException exception) {
            throw new RepoException("Could not empty table");
        }
    }

    @Override
    public void deleteEntity(Conversatie conversatie) {
        try {
            Connection connection = DriverManager.getConnection(url);
            Statement statement = connection.createStatement();

            statement.executeUpdate("DELETE FROM [socialNetwork].Conversatii WHERE idConversatie = " + conversatie.getId() + ";");

        } catch (SQLException exception) {
            throw new RepoException("Could not empty table");
        }
    }

    @Override
    public void updateEntity(Conversatie conversatie) {

    }

    public void incarcaConversatieDacaExista(Utilizator logat, Utilizator testat) {
        try {
            Connection connection = DriverManager.getConnection(url);
            String query = "select * from [socialNetwork].Conversatii " +
                    "where " +
                    "(toConversatie = " + logat.getId() + " and fromConversatie = " + testat.getId() + " ) " +
                    "or " +
                    "(toConversatie = " + testat.getId() + " and fromConversatie = " + logat.getId() + " ) ";
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            {
                while (resultSet.next()) {
                    Conversatie prietenie = this.extractEntity(resultSet);
                    if (!this.entities.containsKey(prietenie.getId())) {
                        super.superSave(prietenie);
                    }
                }
            }
            connection.close();
        } catch (SQLException exception) {
            throw new RepoException("The database connection failed");
        }
    }

    public void incarcaToateConversatiile(Utilizator logat) {
        try {
            Connection connection = DriverManager.getConnection(url);
            String query = "select * from [socialNetwork].Conversatii " +
                    "where " +
                    "(toConversatie = " + logat.getId() +
                    " or " +
                    "fromConversatie = " + logat.getId() + " )";
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            this.entities.clear();
            {
                while (resultSet.next()) {

                    Conversatie prietenie = this.extractEntity(resultSet);
                    if (!this.entities.containsKey(prietenie.getId())) {
                        super.superSave(prietenie);
                    }
                }
            }
            connection.close();
        } catch (SQLException exception) {
            throw new RepoException("The database connection failed");
        }
    }
}


