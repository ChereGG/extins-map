package socialnetwork.repository.database;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import socialnetwork.domain.Message;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.RepoException;
import socialnetwork.domain.validators.Validator;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;

public class MesajeDatabase extends AbstractDatabaseRepository<Long, Message> {
    public MesajeDatabase(Validator<Message> validator, String url) {
        super(validator, url);
    }

    @Override
    public void loadData() {
        try {
            Connection connection = DriverManager.getConnection(url);

            PreparedStatement statement = connection.prepareStatement("SELECT * FROM [socialNetwork].Mesaje");
            ResultSet resultSet = statement.executeQuery();
            {
                while (resultSet.next()) {
                    Message message = this.extractEntity(resultSet);
                    super.superSave(message);
                }
            }
            connection.close();
        } catch (SQLException exception) {
            throw new RepoException("The database connection failed");
        }
    }


    @Override
    public Message extractEntity(ResultSet resultSet) {
        try{
            Long idMesaj = resultSet.getLong("idMesaj");
            String[] destinatari=resultSet.getString("mesajTo").split(",");
            ArrayList<Long> mesajTo=new ArrayList<>();
            for(String id: destinatari){
                mesajTo.add(Long.parseLong(id));
            }
            Long mesajFrom=resultSet.getLong("mesajFrom");
            String date=resultSet.getString("mesajData");
            LocalDateTime mesajData=LocalDateTime.parse(date);
            Long mesajReplied=resultSet.getLong("mesajReplied");
            Long mesajReply=resultSet.getLong("mesajReply");
            String mesajText=resultSet.getString("mesajText");
            String mesajGroupName=resultSet.getString("mesajGroupName");
            String mesajGroupPicture= resultSet.getString("mesajGroupPicture");
            Message message=null;
            if(!mesajGroupName.equals("null") && !mesajGroupPicture.equals("null")) {
                ImageView imageView=new ImageView(new Image(mesajGroupPicture));
                message=new Message(mesajFrom,mesajTo,mesajText,mesajData,mesajGroupName,imageView,mesajGroupPicture);
            }
            else {
                message = new Message(mesajFrom, mesajTo, mesajText, mesajData);
            }
            message.setId(idMesaj);
            if(mesajReplied!=0){
                message.setRepliedMessage(mesajReplied);
            }
            if(mesajReply!=0){
                message.setReply(mesajReply);
            }
            return message;


        }
        catch (SQLException exception) {
            throw new RepoException("Could not extract entity from table. Check if the fields' values are valid!");
        }
    }

    @Override
    public void writeToDatabase(Message message) {
        try{
            Connection connection = DriverManager.getConnection(url);
            Statement statement = connection.createStatement();
            String s2=new String("");
            for(Long id:message.getTo()){
                s2+=id+",";
            }
            String s=new String("");
            for(int i=0;i<s2.length()-1;i++){
                s+=s2.charAt(i);
            }
            Long replied=message.getRepliedMessage();
            if(replied==null){
                replied=Long.parseLong("0");
            }
            Long reply=message.getReply();
            if(reply==null){
                reply=Long.parseLong("0");
            }
            String groupName;
            String groupPicture;
            if(message.getGroupName()==null){
                groupName="null";
            }
            else
            {
                groupName=message.getGroupName();
            }
            if(message.getPicturePath()==null){
                groupPicture="null";
            }
            else
            {
                groupPicture=message.getPicturePath();
            }

            statement.executeUpdate("INSERT INTO [socialNetwork].Mesaje(idMesaj,mesajFrom,mesajTo,mesajText,mesajData,mesajReplied,mesajReply)"+
                    " VALUES ("+message.getId().toString()+","+message.getFrom().toString()+",'"+s+"','"+message.getMessage()+"','"+message.getDate().toString()+"',"+
                    replied+","+reply+",'"+groupName+"','"+groupPicture+"')");
            connection.close();
        }
        catch (SQLException exception)
        {
            throw new RepoException("Could not write to table!");
        }
    }

    @Override
    public void emptyTable() {
        try{
            Connection connection = DriverManager.getConnection(url);
            Statement statement = connection.createStatement();

            statement.executeUpdate("DELETE FROM [socialNetwork].Mesaje");
        }
        catch (SQLException exception)
        {
            throw new RepoException("Could not empty table");
        }
    }

    @Override
    public void deleteEntity(Message message) {
        try{
            Connection connection = DriverManager.getConnection(url);
            Statement statement = connection.createStatement();

            statement.executeUpdate("DELETE FROM [socialNetwork].Mesaje  WHERE idMesaj = "+message.getId()+";");
        }
        catch (SQLException exception)
        {
            throw new RepoException("Could not empty table");
        }
    }

    @Override
    public void updateEntity(Message message) {

    }
}
