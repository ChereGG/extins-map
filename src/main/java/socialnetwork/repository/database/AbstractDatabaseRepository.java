package socialnetwork.repository.database;

import socialnetwork.domain.Entity;
import socialnetwork.domain.validators.RepoException;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.memory.InMemoryRepository;

import java.sql.*;

public abstract class AbstractDatabaseRepository<ID, E extends Entity<ID>> extends InMemoryRepository<ID,E> {
    protected  String url;

    public AbstractDatabaseRepository(Validator<E> validator, String url) {
        super(validator);
        this.url = url;
        //loadData();
    }

    public abstract void loadData();

    public abstract E extractEntity(ResultSet resultSet);

    public abstract void writeToDatabase(E e);

    public abstract void emptyTable();
    public abstract  void deleteEntity(E e);
    public abstract void updateEntity(E e);

    @Override
    public E save(E entity){
        if(entity.getId()!=null) {
            E e = super.save(entity);
            if (e == null) {
                writeToDatabase(entity);
            }
            return e;

        }
        else{
                super.validator.validate(entity);
                writeToDatabase(entity);
                E e=super.save(entity);
                return e;
        }

    }

    @Override
    public E delete(ID id) {
        E e = super.delete(id);
        this.deleteEntity(e);
        return e;
    }
    public void reloadDataBase(){
        emptyTable();
        entities.values().forEach(this::writeToDatabase);
    }
    @Override
    public E update(E entity) {
        this.updateEntity(entity);
        E e = super.update(entity);

        return e;
    }

    @Override
    public void DOOM() {
        super.DOOM();
    }

    protected E superSave(E e){
        return super.save(e);
    }
}