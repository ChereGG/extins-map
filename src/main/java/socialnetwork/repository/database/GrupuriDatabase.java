package socialnetwork.repository.database;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import socialnetwork.domain.Conversatie;
import socialnetwork.domain.Grup;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.RepoException;
import socialnetwork.domain.validators.Validator;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class GrupuriDatabase extends AbstractDatabaseRepository<Long, Grup> {
    public GrupuriDatabase(Validator<Grup> validator, String url) {
        super(validator, url);
        this.loadData();
    }

    @Override
    public void loadData() {
        try {
            Connection connection = DriverManager.getConnection(url);

            PreparedStatement statement = connection.prepareStatement("SELECT * FROM [socialNetwork].Grupuri");
            ResultSet resultSet = statement.executeQuery();
            {
                while (resultSet.next()) {
                    Grup message = this.extractEntity(resultSet);
                    super.superSave(message);
                }
            }
            connection.close();
        } catch (SQLException exception) {
            throw new RepoException("The database connection failed");
        }
    }

    @Override
    public Grup extractEntity(ResultSet resultSet) {
        try {
            Long id = resultSet.getLong("idGrup");
            String[] members =resultSet.getString("membersGrup").split(",");
            ArrayList<Long>membrii=new ArrayList<>();
            for(String s : members){
                membrii.add(Long.parseLong(s));
            }
            String nume=resultSet.getString("numeGrup");
            String picturePath=resultSet.getString("pozaGrup");
            ImageView imageView=new ImageView(new Image(picturePath));
            imageView.setFitHeight(40);
            imageView.setFitWidth(40);
            Grup grup=new Grup(membrii,nume,picturePath,imageView);
            grup.setId(id);
            return grup;
        }
        catch (SQLException exception) {
            throw new RepoException("Could not extract entity from table. Check if the fields' values are valid!");
        }
    }

    @Override
    public void writeToDatabase(Grup grup) {
        try {
            Connection connection = DriverManager.getConnection(url);
            Statement statement = connection.createStatement();
            String s="";
            for(int i=0;i<grup.getMembers().size();i++){
                s+=grup.getMembers().get(i);
                if(i<grup.getMembers().size()-1) {
                    s += ",";
                }
            }
            statement.executeUpdate("INSERT INTO [socialNetwork].Grupuri"+
                    " VALUES ('"+s+"','"+grup.getGroupName()+"','"+grup.getPicturePath()+"')",Statement.RETURN_GENERATED_KEYS);
            ResultSet keys=statement.getGeneratedKeys();
            Long key=null;
            if(keys.next()){
                key=keys.getLong(1);
            }
            grup.setId(key);

        }
        catch (SQLException exception)
        {
            throw new RepoException("Could not write to table!");
        }
    }

    @Override
    public void emptyTable() {
        try{
            Connection connection = DriverManager.getConnection(url);
            Statement statement = connection.createStatement();

            statement.executeUpdate("DELETE FROM [socialNetwork].Grupuri");
        }
        catch (SQLException exception)
        {
            throw new RepoException("Could not empty table");
        }
    }

    @Override
    public void deleteEntity(Grup grup) {
        try{
            Connection connection = DriverManager.getConnection(url);
            Statement statement = connection.createStatement();

            statement.executeUpdate("DELETE FROM [socialNetwork].Grupuri  WHERE idGrup = "+grup.getId()+";");
        }
        catch (SQLException exception)
        {
            throw new RepoException("Could not empty table");
        }
    }

    @Override
    public void updateEntity(Grup grup) {

    }
}
