package socialnetwork.repository.database;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import socialnetwork.domain.Event;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.RepoException;
import socialnetwork.domain.validators.Validator;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class EvenimenteDatabase extends AbstractDatabaseRepository<Long, Event> {
    public EvenimenteDatabase(Validator<Event> validator, String url) {
        super(validator, url);
        this.loadData();
    }

    @Override
    public void loadData() {
        try {
            Connection connection = DriverManager.getConnection(url);
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM [socialNetwork].Evenimente");
            ResultSet resultSet = statement.executeQuery();
            {
                while (resultSet.next()) {
                    Event event = this.extractEntity(resultSet);
                    super.superSave(event);
                }
            }
            connection.close();
        } catch (SQLException exception) {
            throw new RepoException("The database connection failed");
        }

    }

    @Override
    public Event extractEntity(ResultSet resultSet) {
        try {

            Long id = resultSet.getLong("idEveniment");
            String data = resultSet.getString("dataEveniment");
            LocalDateTime date = LocalDateTime.parse(data);
            String[] participanti = resultSet.getString("participantiEveniment").split(",");
            ArrayList<Long> membrii = new ArrayList<>();
            for (String s : participanti) {
                membrii.add(Long.parseLong(s));
            }
            if(membrii.get(0).equals(Long.parseLong("0"))){
                membrii.clear();
            }
            String path = resultSet.getString("pozaEveniment");
            ImageView imageView = new ImageView(new Image(path));
            imageView.setFitHeight(40);
            imageView.setFitWidth(40);
            String descriere=resultSet.getString("descriereEveniment");
            String nume = resultSet.getString("numeEveniment");
            Event event = new Event(nume, date, membrii, path, imageView,descriere);
            event.setId(id);
            return event;
        } catch (SQLException exception) {
            throw new RepoException("Could not extract entity from table. Check if the fields' values are valid!");
        }
    }

    @Override
    public void writeToDatabase(Event event) {
        try {
            Connection connection = DriverManager.getConnection(url);
            Statement statement = connection.createStatement();
            String s="";

            for(int i=0;i<event.getParticipanti().size();i++){
                s+=event.getParticipanti().get(i);
                if(i<event.getParticipanti().size()-1) {
                    s += ",";
                }
            }
            if(event.getParticipanti().size()==0){
                s="0";
            }
            statement.executeUpdate("INSERT INTO [socialNetwork].Evenimente"+
                    " VALUES ('"+event.getData().toString()+"','"+s+"','"+event.getPicturePath()+"','"+event.getNume()+"')",Statement.RETURN_GENERATED_KEYS);
            ResultSet keys=statement.getGeneratedKeys();
            Long key=null;
            if(keys.next()){
                key=keys.getLong(1);
            }
            event.setId(key);

        }
        catch (SQLException exception)
        {
            throw new RepoException("Could not write to table!");
        }
    }

    @Override
    public void emptyTable() {
        try {
            Connection connection = DriverManager.getConnection(url);
            Statement statement = connection.createStatement();
            statement.executeUpdate("DELETE FROM [socialNetwork].Evenimente");
        } catch (SQLException exception) {
            throw new RepoException("Could not empty table");
        }
    }

    @Override
    public void deleteEntity(Event event) {
        try {
            Connection connection = DriverManager.getConnection(url);
            Statement statement = connection.createStatement();
            statement.executeUpdate("DELETE FROM [socialNetwork].Evenimente E WHERE E.idEveniment = " + event.getId() + ";");
        } catch (SQLException exception) {
            throw new RepoException("Could not empty table");
        }
    }

    @Override
    public void updateEntity(Event event) {
        try {
            Connection connection = DriverManager.getConnection(url);
            Statement statement = connection.createStatement();
            String s="";

            for(int i=0;i<event.getParticipanti().size();i++){
                s+=event.getParticipanti().get(i);
                if(i<event.getParticipanti().size()-1) {
                    s += ",";
                }
            }
            if(event.getParticipanti().size()==0){
                s="0";
            }
            String query = "UPDATE [socialNetwork].Evenimente " +
                    "SET " +
                    "dataEveniment = '" + event.getData().toString() + "', " +
                    "participantiEveniment = '" + s + "', " +
                    "pozaEveniment = '" +event.getPicturePath()+ "', " +
                    "numeEveniment = '" + event.getNume() + "'" +
                    " WHERE idEveniment = " + event.getId() + ";";
            statement.executeUpdate(query);
        } catch (SQLException exception) {
            throw new RepoException("Could not update table");
        }
    }
}
