package socialnetwork.service;

import socialnetwork.domain.*;
import socialnetwork.domain.validators.ServiceException;
import socialnetwork.repository.Repository;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Observable;
import java.util.concurrent.atomic.AtomicReference;

public class CereriPrietenieService extends Observable {
    private Repository<Tuple<Long,Long>, Prietenie> prietenieRepo;
    private Repository<Tuple<Long,Long>, CererePrietenie> cererePrietenieRepo;
    private Repository<Long, Utilizator> utilizatorRepo;

    public CereriPrietenieService(Repository<Tuple<Long, Long>, Prietenie> prietenieRepo, Repository<Tuple<Long, Long>, CererePrietenie> cererePrietenieRepo, Repository<Long, Utilizator> utilizatorRepo) {
        this.prietenieRepo = prietenieRepo;
        this.cererePrietenieRepo = cererePrietenieRepo;
        this.utilizatorRepo = utilizatorRepo;
    }


    public Boolean existaCerere(CererePrietenie cererePrietenie)
    {
        AtomicReference<Boolean> exista = new AtomicReference<>(false);
        this.cererePrietenieRepo.findAll().forEach(x -> {
            if(x.getId().equals(cererePrietenie.getId()))
            {
                exista.set(true);
            }
        });
        return exista.get();
    }
    public CererePrietenie addCererePrietenie(CererePrietenie cererePrietenie)
    {
        Utilizator u1 = utilizatorRepo.findOne(cererePrietenie.getId().getLeft());
        Utilizator u2 = utilizatorRepo.findOne(cererePrietenie.getId().getRight());
        if(u1.equals(u2)){
            throw new ServiceException("Nu iti poti trimite singur o cerere de prietenie!");
        }
        if(u1 != null && u2 != null) {
            //Ce facem cu cererile
            //1. Trimit o cerere, dupa ce am fost rejected
            //2. Trimit o cerere dupa ce am dat reject
            //3. Trimit o cerere dupa ce am dat/mi-am luat unfriend
            if(this.existaCerere(cererePrietenie)){
                this.deleteCererePrietenie(cererePrietenie.getId());
            }
            CererePrietenie cererePrietenie1 = cererePrietenieRepo.save(cererePrietenie);
            this.setChanged();
            this.notifyObservers();
            return cererePrietenie1;
        }
        else{
            throw new ServiceException("Both users must exist in order to send a friend request!");
        }
    }

   public CererePrietenie deleteCererePrietenie(Tuple<Long, Long> id)
   {
       CererePrietenie cererePrietenie = cererePrietenieRepo.delete(id);
       this.setChanged();
       this.notifyObservers();
       return cererePrietenie;
   }

   public CererePrietenie updateCererePrietenie(CererePrietenie cererePrietenie)
   {
       CererePrietenie cererePrietenie1 = cererePrietenieRepo.update(cererePrietenie);
       this.setChanged();
       this.notifyObservers();
       return cererePrietenie1;
   }

   public ArrayList<CererePrietenie> cereriDePrietenie(Long idUtilizator)
   {
       ArrayList<CererePrietenie> cererePrietenieArrayList = new ArrayList<>();
       this.cererePrietenieRepo.findAll().forEach(x->{
            if(x.getId().getRight().equals(idUtilizator))
            {
                cererePrietenieArrayList.add(x);
            }
       });
       return cererePrietenieArrayList;
   }

    public ArrayList<CererePrietenie> cereriDePrieteniePending(Long idUtilizator)
    {
        ArrayList<CererePrietenie> cererePrietenieArrayList = new ArrayList<>();
        this.cererePrietenieRepo.findAll().forEach(x->{
            if(x.getId().getRight().equals(idUtilizator) && x.getStatus() == Status.PENDING)
            {
                cererePrietenieArrayList.add(x);
            }
        });
        return cererePrietenieArrayList;
    }

    public Boolean aPrimitCerere(Long reciever, Long sender)
    {
        AtomicReference<Boolean> sentRequest = new AtomicReference<>(false);
        ArrayList<CererePrietenie> list = this.cereriDePrieteniePending(reciever);
        list.forEach(x -> {
            if(x.getId().getLeft().equals(sender))
            {
                sentRequest.set(true);
            }
        });
        return sentRequest.get();
    }
    public void incarcaCerereDacaExista(Utilizator logat, Utilizator testat){
        Object u = null;
        try {
            Method method = this.cererePrietenieRepo.getClass().getMethod("incarcaCerereDacaExista", Utilizator.class,Utilizator.class);
            u = method.invoke(this.cererePrietenieRepo, logat,testat);

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }
    public void DOOM(){
        this.cererePrietenieRepo.DOOM();
    }
}
