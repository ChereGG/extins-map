package socialnetwork.service;

import socialnetwork.domain.Grup;
import socialnetwork.domain.GrupMessage;
import socialnetwork.domain.Utilizator;
import socialnetwork.repository.Repository;
import socialnetwork.repository.database.AbstractDatabaseRepository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Observable;

public class GrupMessageService extends Observable {

    private AbstractDatabaseRepository<Long, Grup> grupRepository;
    private Repository<Long, Utilizator> utilizatorRepository;
    private AbstractDatabaseRepository<Long, GrupMessage> grupMessageRepository;

    public GrupMessageService(AbstractDatabaseRepository<Long, Grup> grupRepository, Repository<Long, Utilizator> utilizatorRepository, AbstractDatabaseRepository<Long, GrupMessage> grupMessageRepository) {
        this.grupRepository = grupRepository;
        this.utilizatorRepository = utilizatorRepository;
        this.grupMessageRepository = grupMessageRepository;
    }
    public GrupMessage addMessage(GrupMessage message){
        GrupMessage c=this.grupMessageRepository.save(message);
        this.setChanged();
        this.notifyObservers();
        return c;
    }
    public Iterable<GrupMessage> getAll(){return  this.grupMessageRepository.findAll();}

    public List<GrupMessage> toateMesajeleDinGrup(Long idGrup){
        List<GrupMessage> result=new ArrayList<>();
        this.grupMessageRepository.findAll().forEach(x->{
            if(x.getGroupId().equals(idGrup))
                result.add(x);
        });
        Comparator<GrupMessage>comparator= Comparator.comparing(GrupMessage::getDate);
        result.sort(comparator);
        return result;
    }

}
