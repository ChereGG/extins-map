package socialnetwork.service;

import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.repository.Repository;


import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Observable;
import java.util.concurrent.atomic.AtomicReference;

public class UtilizatorService extends Observable {
    private final Repository<Long, Utilizator> utilizatorRepo;
    private final Repository<Tuple<Long, Long>, Prietenie> prietenieRepo;

    /**
     * @param utilizatorRepo Repository de tip utilizator
     * @param prietenieRepo  Repository de tip prietenie
     */
    public UtilizatorService(Repository<Long, Utilizator> utilizatorRepo, Repository<Tuple<Long, Long>, Prietenie> prietenieRepo) {
        this.utilizatorRepo = utilizatorRepo;
        this.prietenieRepo = prietenieRepo;
    }

    /**
     * @param utilizator Utilizatorul de adaugat
     * @return null
     */
    public Utilizator addUtilizator(Utilizator utilizator) {

        Utilizator u1 = utilizatorRepo.save(utilizator);
        this.setChanged();
        this.notifyObservers();
        return u1;
    }
    public void addSmecher(Utilizator u){
        this.utilizatorRepo.addSmehcer(u);
    }
    public void hocuspocus() {
        this.setChanged();
        this.notifyObservers();
    }

    /**
     * @param id Id-ul utilizatorului care trebuie sters
     * @return utilizatorul care a fost sters
     */
    public Utilizator deleteUtilizator(Long id) {

        Utilizator utilizator = utilizatorRepo.delete(id);
        if (utilizator != null) {
            utilizatorRepo.findAll().forEach(x -> {
                ArrayList<Long> ids = new ArrayList<>();
                x.getFriends().forEach(y -> {
                    if (y.equals(id)) {
                        ids.add(y);
                    }
                });
                if (!ids.isEmpty()) {
                    x.removeFriend(ids.get(0));
                    Tuple<Long, Long> t = new Tuple<Long, Long>(ids.get(0), x.getId());
                    Tuple<Long, Long> t1 = new Tuple<Long, Long>(x.getId(), ids.get(0));
                    prietenieRepo.delete(t);
                }
            });
        }
        this.setChanged();
        this.notifyObservers();
        return utilizator;
    }

    /**
     * @param utilizator
     * @return
     */
    public Utilizator updateUtilizator(Utilizator utilizator) {
        Utilizator utilizator1 = utilizatorRepo.update(utilizator);
        this.setChanged();
        this.notifyObservers();
        return utilizator1;
    }

    /**
     * @return Un obiect de tip iterabil cu toti utilizatorii
     */
    public Iterable<Utilizator> getAll() {
        return utilizatorRepo.findAll();
    }


    /**
     * Functie care incarca prieteniile fiecarui utilizator
     */
    public void reloadFriends() {
        this.prietenieRepo.findAll().forEach(x -> {
            Utilizator u1 = utilizatorRepo.findOne(x.getId().getLeft());
            Utilizator u2 = utilizatorRepo.findOne(x.getId().getRight());
            u1.addFriend(u2);
            u2.addFriend(u1);
        });
    }

    private void setVisitedFalse() {
        this.getAll().forEach(x -> {
            x.setVisited(false);
        });
    }

    public void DOOM(){
        this.utilizatorRepo.DOOM();
    }

    /**
     * @param id Un id de tip Long
     * @return Utilizatorul cautat
     */
    public Utilizator findOne(Long id) {
        return utilizatorRepo.findOne(id);
    }

    private void DFS(ArrayList<ArrayList<Long>> l, int i, Long id) {
        Utilizator utilizator = this.utilizatorRepo.findOne(id);
        utilizator.setVisited(true);
        l.get(i).add(id);
        utilizator.getFriends().forEach(y -> {
            if (!this.utilizatorRepo.findOne(y).isVisited())
                DFS(l, i, y);
        });


    }

    /**
     * Functie care returneaza numar de grupe
     *
     * @return numarul de grupuri
     */
    public ArrayList<ArrayList<Long>> numberOfGroups() {
        this.setVisitedFalse();
        AtomicReference<Integer> i = new AtomicReference<>(-1);
        ArrayList<ArrayList<Long>> listaDeListe = new ArrayList<ArrayList<Long>>();
        this.getAll().forEach(x -> {
            if (!x.isVisited()) {
                i.set(i.get() + 1);
                listaDeListe.add(new ArrayList<Long>());
                DFS(listaDeListe, i.get(), x.getId());
            }
        });
        return listaDeListe;
    }

    /**
     * Verifica daca exista sau nu utilizaotrul cu id-ul dat
     * @param id
     * @return T/F
     */
    public boolean existaUtilizator(Long id) {
        AtomicReference<Boolean> exista = new AtomicReference<Boolean>(false);
        this.getAll().forEach(x -> {
            if (x.getId().equals(id)) {
                exista.set(true);
            }
        });
        return exista.get();
    }

    /**
     * cauta un utilizator dat in DB
     * @param username
     * @return Userul gasit sau null daca nu s-a gasit nimic
     */
    public Object findOneDataBase(String username) {
        Object u = null;
        try {
            Method method = this.utilizatorRepo.getClass().getMethod("findOneDataBase", String.class);
            u = method.invoke(this.utilizatorRepo, username);

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return u;
    }

    public Object findOneDataBaseId(Long id) {
        Object u = null;
        try {
            Method method = this.utilizatorRepo.getClass().getMethod("findOneDataBaseId", Long.class);
            u = method.invoke(this.utilizatorRepo, id);

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return u;
    }

    public void bagaTotiPrietenii(Utilizator logat){
        Object u = null;
        try {
            Method method = this.utilizatorRepo.getClass().getMethod("bagaTotiPrietenii", Utilizator.class);
            u = method.invoke(this.utilizatorRepo, logat);

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }
    public boolean incarcaPrimii(Utilizator curent) {
        Object u = null;
        try {
            Method method = this.utilizatorRepo.getClass().getMethod("incarcaPrimii", Utilizator.class);
            u = method.invoke(this.utilizatorRepo, curent);

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return (boolean) u;
    }
    public boolean incarcaNext(Utilizator lastOne,Utilizator curent){
        Object u = null;
        try {
            Method method = this.utilizatorRepo.getClass().getMethod("incarcaNext", Utilizator.class,Utilizator.class);
            u = method.invoke(this.utilizatorRepo, lastOne,curent);

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return (boolean) u;
    }

    public boolean incarcaPrev(Utilizator lastOne,Utilizator curent){
        Object u = null;
        try {
            Method method = this.utilizatorRepo.getClass().getMethod("incarcaPrev", Utilizator.class,Utilizator.class);
            u = method.invoke(this.utilizatorRepo, lastOne,curent);

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return (boolean) u;
    }

    public boolean incarcaPrimiiPrieteni(Utilizator logat){
        Object u = null;
        try {
            Method method = this.utilizatorRepo.getClass().getMethod("incarcaPrimiiPrieteni",Utilizator.class);
            u = method.invoke(this.utilizatorRepo, logat);

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return (boolean) u;
    }

    public boolean incarcaUrmatoriiPrieteni(Utilizator logat,Utilizator lastOne){
        Object u = null;
        try {
            Method method = this.utilizatorRepo.getClass().getMethod("incarcaUrmatoriiPrieteni",Utilizator.class,Utilizator.class);
            u = method.invoke(this.utilizatorRepo, logat,lastOne);

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return (boolean) u;
    }
    public boolean incarcaAnterioriiPrieteni(Utilizator logat,Utilizator lastOne){
        Object u = null;
        try {
            Method method = this.utilizatorRepo.getClass().getMethod("incarcaAnterioriiPrieteni",Utilizator.class,Utilizator.class);
            u = method.invoke(this.utilizatorRepo, logat,lastOne);

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return (boolean) u;
    }


    public boolean incarcaPrimiiCereri(Utilizator logat){
        Object u = null;
        try {
            Method method = this.utilizatorRepo.getClass().getMethod("incarcaPrimiiCereri",Utilizator.class);
            u = method.invoke(this.utilizatorRepo, logat);

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return (boolean) u;
    }

    public boolean incarcaUrmatoriiCereri(Utilizator logat,Utilizator lastOne){
        Object u = null;
        try {
            Method method = this.utilizatorRepo.getClass().getMethod("incarcaUrmatoriiCereri",Utilizator.class,Utilizator.class);
            u = method.invoke(this.utilizatorRepo, logat,lastOne);

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return (boolean) u;
    }
    public boolean incarcaAnterioriiCereri(Utilizator logat,Utilizator lastOne){
        Object u = null;
        try {
            Method method = this.utilizatorRepo.getClass().getMethod("incarcaAnterioriiCereri",Utilizator.class,Utilizator.class);
            u = method.invoke(this.utilizatorRepo, logat,lastOne);

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return (boolean) u;
    }

    public boolean incarcaPrimiiFiltrati(Utilizator curent, String filtr) {
        Object u = null;
        try {
            Method method = this.utilizatorRepo.getClass().getMethod("incarcaPrimiiFiltrati", Utilizator.class, String.class);
            u = method.invoke(this.utilizatorRepo, curent,filtr);

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return (boolean) u;
    }
    public boolean incarcaNextFiltrati(Utilizator lastOne,Utilizator curent, String filtr){
        Object u = null;
        try {
            Method method = this.utilizatorRepo.getClass().getMethod("incarcaNextFiltrati", Utilizator.class,Utilizator.class, String.class);
            u = method.invoke(this.utilizatorRepo, lastOne,curent,filtr);

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return (boolean) u;
    }

    public boolean incarcaPrevFiltrati(Utilizator lastOne,Utilizator curent, String filtr){
        Object u = null;
        try {
            Method method = this.utilizatorRepo.getClass().getMethod("incarcaPrevFiltrati", Utilizator.class,Utilizator.class, String.class);
            u = method.invoke(this.utilizatorRepo, lastOne,curent,filtr);

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return (boolean) u;
    }

}
/*    public ArrayList<Long> mostSociableCommunity(){
        ArrayList<ArrayList<Long>> listaDeListe = numberOfGroups();
        ArrayList<Long> rataPrietenie = new ArrayList<>();
        AtomicReference<Integer> count = new AtomicReference<>(0);
        int[] i= new int[1];
        i[0] =0;

        listaDeListe.forEach(x -> {
                x.forEach(y -> {
                    Utilizator utilizator = this.findOne(y);
                    count.set(count.get() + utilizator.getFriends().size());
                });
                int nrUtilizatori = x.size();
                rataPrietenie.set(i[0]++, count.get() / nrUtilizatori*(nrUtilizatori-1) * 2 * 100);
                count.set(0);
        });
        int lungimeMaxima = 0;
        int maxim = 0;
        int imaxim = 0;
        for (int j = 0; j < rataPrietenie.size(); j++)
        {
            if (rataPrietenie.get(j) > maxim)
            {
                maxim = rataPrietenie.get(j);
                imaxim = j;
            }
        }
        return rataPrietenie;
    }*/

